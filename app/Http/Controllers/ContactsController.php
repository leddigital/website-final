<?php

namespace App\Http\Controllers;

use App\Contacts;
use Illuminate\Http\Request;

class ContactsController extends Controller
{
    protected $model;

    public function __construct()
    {
        $this->model = new Contacts();
    }

    public function index() {
        return view('admin.content.contato.index');
    }

    public function form(Request $request) {

        $id = $request->route('id');
        if(isset($id) && $id != "") {
            $entity = $this->model->where('id', '=', $id)->get()->first();
            return view('admin.content.contato.form', ['entity' => $entity]);
        } else
            return view('admin.content.contato.form');

    }

    public function readAll(Request $request) {

        $collection = $this->model->get()->all();
        $data['data'] = $collection;
        echo json_encode($data);

    }

    public function save (Request $request) {

        $form = $request->all();
        $id = $request->route('id');

        if(isset($id) && $id != "") {

            $entity = $this->model->find($id);
            $form['estado'] = $this->getStateName($form['state']);

            if($entity->update($form)) {
                $res = [
                    'status' => 200,
                    'data' => $entity,
                ];
            } else {
                $res = [
                    'status' => 500,
                    'data' => $entity,
                ];
            }

        } else {

            $form['estado'] = $this->getStateName($form['state']);
            if($entity = $this->model->create($form)){
                
                $res = [
                    'status' => 200,
                    'data' => $entity,
                ];

            } else {

                $res = [
                    'status' => 500,
                    'data' => $entity,
                ];

            }

        }
        return response()->json($res);
    }

    public function delete(Request $request) {

        $id = $request->route('id');
        $entity = $this->model->find($id);
        $entity->delete();

    }

}
