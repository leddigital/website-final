<?php

namespace App\Http\Controllers;

use App\Banners;
use App\Contacts;
use App\Posts;
use App\Contents;
use App\Informations;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;

class NavigationController extends Controller
{

    public function __construct()
    {
        $informations = Informations::get()->first();

        // Verifica se está linkado o vídeo do youtube e recupera somente o id do link
        if(isset($informations->video) && ($informations->video!=""))
        $informations->video = $this->getYouTubeVideoId($informations->video);

        View::share('informations', $informations);
    }

    public function index(Request $request) {

        $banners = Banners::all();
        $latestNews = Posts::orderby('created_at', 'DESC')->get()->take(4);
         
        return view('pages.index', 
            ['banners' => $banners, 'latestsNews' => $latestNews]);
    }

    public function bionat(Request $request) {
        return view('pages.bionat');
    }

    public function bioforcas(Request $request) {
        $bioforcas = Contents::get()->all();
        return view('pages.bioforcas', ['bioforcas' => $bioforcas]);
    }

    public function bioforca(Request $request) {
        $url = $request->route('url');
        $bioforca = Contents::where('url', $url)->get()->first();

        return view('pages.bioforca', ['bioforca' => $bioforca]);
    }

    public function bionews(Request $request) {
        $posts = Posts::paginate(6);
        return view('pages.bionews', ['posts' => $posts]);
    }

    public function singlepost(Request $request) {
        $url = $request->route('url');
        $post = Posts::where('url', $url)->get()->first();
        $latestsNews = Posts::where('url', "<>", $url)->orderBy('created_at', 'DESC')->take(3)->paginate(9);

        return view('pages.singlepost', ['post' => $post, 'latestsNews' => $latestsNews]);
    }

    public function procurar(Request $request) {

        $term = $request->get('s');
        $posts = Posts::where('title', 'like', '%' . $term . '%')->paginate(6);

        return view('pages.bionews', ['posts' => $posts]);

    }

    public function mip(Request $request) {
        return view('pages.mip');
    }

    public function contato(Request $request) {
        return view('pages.contato');
    }

    public function presenca(request $request) {
        
        $estados = Contacts::orderBy('state', 'asc')->get()->groupBy('state');
        return view('pages.presenca', ['estados' => $estados]);
    }

    public function biblionat(Request $request) {

        $contatos = Contacts::get()->all();
        return view('pages.biblionat');
    }

    public function calculonat(Request $request) {

        $contatos = Contacts::get()->all();
        return view('pages.calculonat');
    }


}
