<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Contents extends Model
{
    public $timestamps = true;

    protected $fillable = [
        'title',
        'description',
        'short_description',
        'content',
        'image',
        'type',
        'url',
        'categoria',
        'embalagem',
        'composicao',
        'recomendacao',
        'bula',
        'desc_recomendacao',
        'desc_composicao',
        'desc_utilizar',
        'desc_funciona',
        'desc_aplicar',
        'desc_compatibilidade',
        'desc_armazenamento',
        'desc_transporte',
        'desc_cultura',
        'desc_quando'
    ];

    public $rules = [
        'title' => 'required',
        'type'=>'required',
    ];

    public function images(){
        return $this->hasMany('App\ContentsImages');
    }

    public function categories(){
        return $this->belongsToMany('App\Categories','categories_has_contents');
    }
}
