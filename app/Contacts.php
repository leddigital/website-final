<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Contacts extends Model
{
    public $timestamp = true;

    protected $fillable = [
        'name',
        'role',
        'email',
        'contact',
        'address',
        'state',
        'estado'
    ];

    public $rules = [
        'name' => 'required',
        'role' => 'required',
        'email' => 'required',
        'contact' => 'required',
        'address' => 'required',
        'state' => 'required'
    ];
}
