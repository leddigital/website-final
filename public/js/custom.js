(function ($) {
    "use strict";

    $(function () {
        var masterslider_d1da = new MasterSlider();

        // slider controls
        masterslider_d1da.control('arrows', {
            autohide: true,
            overVideo: true
        });
        masterslider_d1da.control('bullets', {
            autohide: false,
            overVideo: true,
            dir: 'h',
            align: 'bottom',
            space: 6,
            margin: 25
        });
        // slider setup
        masterslider_d1da.setup("slider_1", {
            width: 1140,
            height: 800,
            minHeight: 0,
            space: 0,
            start: 1,
            grabCursor: false,
            swipe: true,
            mouse: true,
            keyboard: true,
            layout: "fullwidth",
            wheel: false,
            autoplay: false,
            instantStartLayers: false,
            mobileBGVideo: false,
            loop: true,
            shuffle: false,
            preload: 0,
            heightLimit: true,
            autoHeight: false,
            smoothHeight: true,
            endPause: false,
            overPause: true,
            fillMode: "fill",
            centerControls: true,
            startOnAppear: false,
            layersMode: "center",
            autofillTarget: "",
            hideLayers: false,
            fullscreenMargin: 0,
            speed: 20,
            dir: "h",
            parallaxMode: 'swipe',
            view: "basic"
        });

        $("head").append("<link rel='stylesheet' id='ms-fonts'  href='http://fonts.googleapis.com/css?family=Montserrat:regular,700%7CCrimson+Text:regular' type='text/css' media='all' />");

        window.masterslider_instances = window.masterslider_instances || {};
        window.masterslider_instances["5_d1da"] = masterslider_d1da;
    });

})(jQuery);

// Ajax do formulário de contato

$('#contact-form').submit(function (e) {
    e.preventDefault();

    $.ajax({
        type: $(this).attr('method'),
        url: $(this).attr('action'),
        data: $(this).serialize(),
        beforeSend: function (data) {
            $('#gif-contato').css('display', 'block');
            $("#contact-form :input").prop("disabled", true);
        },
        success: function (data) {
            $('#gif-contato').css('display', 'none');
            $('#status-msg').text('Mensagem enviada com sucesso!')
        },
        error: function (data) {
            $('#gif-contato').css('display', 'none');
            $('#status-msg').text('Falha ao enviar mensagem. Tente novamente mais tarde.')
        }
    })
});

// JS do fadeIn da página inicial
$('#caret-down-home').on('click', function () {
    if ($('.content-home').hasClass('show')) {
        $('.content-home').fadeOut('slow');
        $(this).removeClass('fa-caret-up');
        $(this).addClass('fa-caret-down');
        $('.content-home').removeClass('show');
    } else {
        $('.content-home').fadeIn('slow');
        $(this).removeClass('fa-caret-down');
        $(this).addClass('fa-caret-up');
        $('.content-home').addClass('show');
    }
});

$('#link-content-home').on('click', function () {
    if ($('.content-home').hasClass('show')) {
        $('.content-home').fadeOut('slow');
        $('#caret-down-home').removeClass('fa-caret-up');
        $('#caret-down-home').addClass('fa-caret-down');
        $('.content-home').removeClass('show');
    } else {
        $('.content-home').fadeIn('slow');
        $('#caret-down-home').removeClass('fa-caret-down');
        $('#caret-down-home').addClass('fa-caret-up');
        $('.content-home').addClass('show');
    }
});

// JS do Carrossel das logos das empresas parceiras
$('.empresas').slick({
    centerMode: true,
    centerPadding: '60px',
    slidesToShow: 3,
    autoplay: true,
    autoplaySpeed: 2000,
    responsive: [{
            breakpoint: 768,
            settings: {
                arrows: false,
                centerMode: true,
                centerPadding: '40px',
                slidesToShow: 3
            }
        },
        {
            breakpoint: 480,
            settings: {
                arrows: false,
                centerMode: true,
                centerPadding: '40px',
                slidesToShow: 1
            }
        }
    ]
});

// JS do Nivo Slider
$(window).load(function () {
    $('#sliderNivo').nivoSlider({
        effect: 'fade',
        controlNav: false,
        directionNav: false,
        animSpeed: 500,
        pauseTime: 7000,
        directionNav: true,
        autoplay: true,
        pauseOnHover: false,
    });
});



// JS da tabela de informações
$(document).ready(function(){
    $("#stick-port-inner").sticky({
        topSpacing:100,
        bottomSpacing:400,
    });
});

// var viewportHeight = $(window).height();
// var viewportWIdth = $(window).width();

// $(window).scroll(function(){
//     var height = $(this).scrollTop();
//     var maxHeight = Math.floor($(".content-wrapper").height());
    
//     if (viewportWIdth > 768) {

//         if (height < (maxHeight - viewportHeight)) {
//             $('#portfolio-content').css('top', height);
//         }
//     }
    
// });