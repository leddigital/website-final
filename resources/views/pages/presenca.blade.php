@extends('layouts.site')
@section('title', 'Presença - Bionat')
@section('page-title', 'EQUIPE COMERCIAL')
@section('page-subtitle', 'Você pode contar com a Bionat em diversas localidades do Brasil. Encontre abaixo o contato mais próximo de você.')
@section('content')

@include('blocks.mainbanner')

<div class="body-wrapper float-menu">

    <!-- is search -->

    <div class="content-wrapper">
        <div class="greennature-content">

            
          <!-- Above Sidebar Section-->
          <div class="above-sidebar-wrapper">
            <section id="content-section-1" style="background: #fff;">
                <div class="section-container container">

                    <div class="eight columns">
                        <div class="greennature-item greennature-accordion-item style-1" style="margin-bottom: 60px;">
                
                            @foreach ($estados as $states)
                                <div class="accordion-tab {{ $loop->index == 0?'active pre-active':'' }}">
                                    <h4 class="accordion-title"><i class="{{ $loop->index == 0?'icon-minus':'icon-plus' }}"></i><span>{{ $states[0]->estado }}</span></h4>
                                    <div class="accordion-content">
                                        @foreach ($states as $contato)
                                            <ul style="list-style:none;">
                                                <li><h5 style="margin-bottom:.5rem;">{{ $contato->name }}</h5></li>
                                                <li><strong> E-mail: </strong><span><a href="mailto:{{ $contato->email }}" target="_blank">{{ $contato->email }}</a></span></li>
                                                <li><strong> Telefone: </strong><span><a href="https://wa.me/55<?= preg_replace('/[^0-9]+/','', $contato->contact)?>" target="_blank">{{ $contato->contact }}</a></span></li>
                                                <li><strong> Cidade:  </strong><span>{{ $contato->address }}</span></li>
                                            </ul>
                                        @endforeach
                                    </div>
                                </div>
                            @endforeach

                        </div>
                    </div>
                    <div class="four columns">
                        <div class="greennature-item-title-wrapper greennature-item  greennature-left greennature-small ">
                            <div class="greennature-item-title-container container">
                                <div class="greennature-item-title-head">
                                    <h3 class="greennature-item-title greennature-skin-title greennature-skin-border">Tem alguma dúvida? Pergunta pra gente.</h3>
                                    <div class="clear"></div>
                                </div>
                            </div>
                        </div>
                        <div class="greennature-item greennature-content-item">
                            <div role="form" class="wpcf7" id="wpcf7-f4-o1" lang="pt-BR" dir="ltr">
                                <div class="screen-reader-response"></div>
                                <form action="#" method="post" class="wpcf7-form" novalidate="novalidate">

                                    <p>Seu nome (obrigatório)
                                        <br />
                                        <span class="wpcf7-form-control-wrap your-name"><input type="text" name="your-name" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required" aria-required="true" aria-invalid="false" /></span> </p>
                                    <p>E-mail (obrigatório)
                                        <br />
                                        <span class="wpcf7-form-control-wrap your-email"><input type="email" name="your-email" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-email wpcf7-validates-as-required wpcf7-validates-as-email" aria-required="true" aria-invalid="false" /></span> </p>
                                    <p>Assunto
                                        <br />
                                        <span class="wpcf7-form-control-wrap your-subject"><input type="text" name="your-subject" value="" size="40" class="wpcf7-form-control wpcf7-text" aria-invalid="false" /></span> </p>
                                    <p>Sua mensagem
                                        <br />
                                        <span class="wpcf7-form-control-wrap your-message"><textarea name="your-message" cols="40" rows="10" class="wpcf7-form-control wpcf7-textarea" aria-invalid="false"></textarea></span> </p>
                                    <p>
                                        <input type="submit" value="Enviar" class="wpcf7-form-control wpcf7-submit" disabled/>
                                    </p>
                                </form>
                            </div>
                        </div>
                    </div>
                    <div class="clear"></div>
                </div>
            </section>
        </div>


            

        </div>
        <!-- greennature-content -->
        <div class="clear"></div>
    </div>
    <!-- content wrapper -->

    @endsection
