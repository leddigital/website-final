@extends('layouts.site')
@section('title', 'Bionat - Soluções Biológicas')
@section('content')

<div class="body-wrapper float-menu">

    <!-- is search -->
    <div class="content-wrapper">
        <div class="greennature-content">

            <!-- Above Sidebar Section-->

            <!-- Sidebar With Content Section-->
            <div class="with-sidebar-wrapper">
                <section id="content-section-1">
                    <div class="greennature-full-size-wrapper gdlr-show-all no-skin"
                        style="padding-bottom: 0px;  background-color: #ffffff; ">
                        <div class="greennature-master-slider-item greennature-slider-item greennature-item"
                            style="margin-bottom: 0px;">
                            
                            <!-- MasterSlider -->
                            <div id="P_slider_1" class="master-slider-parent ms-parent-id-1">

                                <!-- MasterSlider Main -->
                                <div id="slider_1" class="master-slider ms-skin-default">

                                    @foreach ($banners as $banner)

                                    <div class="ms-slide" data-delay="7" data-fill-mode="fill">
                                        <img src="{{ asset('plugins/masterslider/public/assets/css/blank.gif') }}"
                                            alt="" title="" data-src="{{ asset('img/banners/' . $banner->image) }}" />

                                        @if (isset($banner->button_link)!="")

                                        <a href="{{ $banner->button_link }}" target="_self"
                                            style="{{ isset($banner->button_bg_color)!=""?"background-color:$banner->button_bg_color;":"" }}
                                               {{ isset($banner->button_text_color)!=""?"color:$banner->button_text_color;":"" }}"
                                            class="ms-layer ms-btn ms-btn-round ms-btn-n msp-preset-btn-159"
                                            data-effect="t(true,n,n,-500,n,n,n,n,n,n,n,n,n,n,n)" data-duration="400"
                                            data-delay="987" data-ease="easeOutQuint" data-type="button"
                                            data-offset-x="1" data-offset-y="208"
                                            data-origin="{{ $banner->text_align }}"
                                            data-position="normal">{{ $banner->button_text }}</a>

                                        @endif

                                        @if (isset($banner->text_2)!="")

                                        <div class="ms-layer  msp-cn-1-3" style="{{ isset($banner->font_color_2)!=""?"color:$banner->font_color_2;":"" }}
                                               {{ isset($banner->font_weight_2)!=""?"font-weight:$banner->font_weight_2;":"" }}
                                               font-size: 50px;" data-effect="t(true,150,n,n,n,n,n,n,n,n,n,n,n,n,n)"
                                            data-duration="437" data-delay="625" data-ease="easeOutQuint"
                                            data-offset-x="0" data-offset-y="105"
                                            data-origin="{{ $banner->text_align }}" data-position="normal">
                                            {{ $banner->text_2 }}</div>

                                        @endif

                                        @if (isset($banner->text_1)!="")

                                        <div class="ms-layer  msp-cn-1-2"
                                            style="{{ isset($banner->font_color_1)!=""?"color:$banner->font_color_1;":"" }}
                                               {{ isset($banner->font_weight_1)!=""?"font-weight:$banner->font_weight_1;":"" }}"
                                            data-effect="t(true,150,n,n,n,n,n,n,n,n,n,n,n,n,n)" data-duration="425"
                                            data-delay="325" data-ease="easeOutQuint" data-offset-x="0"
                                            data-offset-y="-5" data-origin="{{ $banner->text_align }}"
                                            data-position="normal">
                                            {{ $banner->text_1 }}</div>

                                        @endif

                                    </div>

                                    @endforeach

                                </div>
                                <!-- END MasterSlider Main -->

                            </div>
                            <!-- END MasterSlider -->

                        </div>
                        <div class="clear"></div>
                        <div class="clear"></div>
                    </div>
                    <div class="clear"></div>
                </section>

                <section id="content-section-7">
                    <div class="greennature-parallax-wrapper greennature-background-image gdlr-show-all greennature-skin-newsletter"
                        data-bgspeed="0" id="greennature-parallax-wrapper-2"
                        style="background:rgb(37, 53, 38); padding-top: 50px; padding-bottom: 10px; ">
                        <div class="container">
                            <div class="greennature-title-item" style="margin-bottom: 45px;">
                                <div
                                    class="greennature-item-title-wrapper greennature-item  greennature-center greennature-large ">
                                    <div class="greennature-item-title-container container">
                                        <div class="greennature-item-title-head">
                                            <h3 id="link-content-home" style="cursor:pointer;display:inline"
                                                class="greennature-item-title greennature-skin-title greennature-skin-border">
                                                Por que usar soluções biológicas?</h3><i id="caret-down-home"
                                                style="font-size: 24px;margin-left:2.5rem;color:#fec428;cursor:pointer;"
                                                class="fa fa-caret-down" aria-hidden="true"></i>

                                            <div style="display:none;" class="content-home">
                                                <p style="margin-top:2.5rem; text-align:left; font-size:16px;">O
                                                    objetivo do controle biológico é controlar as pragas agrícolas e os
                                                    insetos transmissores de doenças a partir do uso de seus inimigos
                                                    naturais, que podem ser outros insetos, predadores, parasitoides, e
                                                    microrganismos, como fungos, vírus e bactérias.</p>
                                                <p style="text-align:left; font-size:16px;">Método de controle que não
                                                    deixa resíduos nos alimentos e são inofensivos ao meio ambiente e à
                                                    saúde das pessoas.</p>
                                                <p style="text-align:left; font-size:16px;">Por isso são chamadas de
                                                    soluções biológicas, pois não contêm, em sua formulação, nenhuma
                                                    substância química.</p>

                                                <a class="action-ads-button large greennature-button"
                                                    href="{{ route('nav.bioforcas') }}"
                                                    style="margin-top: 20px;color: #ffffff;background-color: #5dc269;">Mais
                                                    informações</a>
                                            </div>

                                            <div class="clear"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="clear"></div>
                </section>

                <section id="content-section-2">
                    <div class="greennature-color-wrapper  gdlr-show-all greennature-skin-brown-column-service"
                        style="background-color: #2d2418;  border-top: 5px solid #3f3221; padding-top: 0px; padding-bottom: 0px; ">
                        <div class="container">
                            <div class="four columns">
                                <div class="greennature-ux column-service-ux">
                                    <div class="greennature-item greennature-column-service-item greennature-type-2"
                                        style="margin-bottom: 0px;">
                                        <div class="column-service-image">
                                            <img src="{{ asset('img/icon-service-1.png') }}" alt="" width="80"
                                                height="80" />
                                        </div>
                                        <div class="column-service-content-wrapper">
                                            <h3 class="column-service-title">Bioforças</h3>
                                            <div class="column-service-content greennature-skin-content">
                                                <p>Soluções biológicas para o combate de pragas e doenças. É eficiente e
                                                    não agride o meio ambiente.</p>
                                            </div>
                                            {{-- <a class="column-service-read-more" href="#">Learn More</a> --}}
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="four columns">
                                <div class="greennature-ux column-service-ux">
                                    <div class="greennature-item greennature-column-service-item greennature-type-2-bg"
                                        style="margin-bottom: 0px;background-color: #3f3221;">
                                        <div class="column-service-image">
                                            <img src="{{ asset('img/icon-service-2.png') }}" alt="" width="80"
                                                height="80" />
                                        </div>
                                        <div class="column-service-content-wrapper">
                                            <h3 class="column-service-title">Performance</h3>
                                            <div class="column-service-content greennature-skin-content">
                                                <p>A solução biológica é mais uma das evoluções da agricultura. Eficaz
                                                    no controle de pragas, já é realidade para os agricultores
                                                    brasileiros. Fale com a Bionat e sabia como essa solução pode ser
                                                    útil para a sua cultura.</p>
                                            </div>
                                            {{-- <a class="column-service-read-more" href="#">Learn More</a> --}}
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="four columns">
                                <div class="greennature-ux column-service-ux">
                                    <div class="greennature-item greennature-column-service-item greennature-type-2"
                                        style="margin-bottom: 0px;">
                                        <div class="column-service-image">
                                            <img src="{{ asset('img/icon-service-3.png') }}" alt="" width="80"
                                                height="80" />
                                        </div>
                                        <div class="column-service-content-wrapper">
                                            <h3 class="column-service-title">Resultados comprovados</h3>
                                            <div class="column-service-content greennature-skin-content">
                                                <p>Parceria com as maiores e mais conceituadas empresas de pesquisas
                                                    microbiológicas do Brasil para oferecer soluções eficazes no combate
                                                    a pragas e doenças. </p>
                                            </div>
                                            {{-- <a class="column-service-read-more" href="#">Learn More</a> --}}
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="clear"></div>
                        </div>
                    </div>
                    <div class="clear"></div>
                </section>

                <section id="content-section-3">
                    <div class="greennature-parallax-wrapper greennature-background-image gdlr-show-all no-skin"
                        id="greennature-parallax-wrapper-1" data-bgspeed="0.11"
                        style="background-image: url('{{ asset('img/bionat_bg.jpg') }}'); padding-top: 90px; padding-bottom: 65px; ">
                        <div class="container">
                            <div class="six columns">
                                <div class="greennature-item greennature-action-ads-item"
                                    style="background-image: url('{{ asset('img/bioforca-home.jpg') }}">
                                    <h3 class="action-ads-title" style="color: #facc2e;">Bioforças</h3>
                                    <div class="action-ads-caption greennature-skin-info">Não agride o meio ambiente
                                    </div>
                                    <div class="action-ads-divider" style="background: #facc2e;"></div>
                                    <div class="action-ads-content action-ads-content-personalizado">
                                        <p>Soluções biológicas para o Manejo Integrado de pragas e doenças.</p>
                                        <a class="action-ads-button large greennature-button"
                                            href="{{ route('nav.bioforcas') }}"
                                            style="color: #6d5b1c;background-color: #fec428;">Ver mais</a>
                                    </div>
                                </div>
                            </div>
                            <div class="six columns">
                                <div class="greennature-item greennature-action-ads-item"
                                    style="background-image: url('{{ asset('img/mip-home.jpg') }}')">
                                    <h3 class="action-ads-title" style="color: #5dc269;">Manejo Integrado de Pragas</h3>
                                    <div class="action-ads-caption greennature-skin-info">MIP</div>
                                    <div class="action-ads-divider" style="background: #5dc269;"></div>
                                    <div class="action-ads-content">
                                        <p>Quando bem empregada, a técnica do Manejo Integrado de pragas e doenças (MIP)
                                            limita os efeitos potenciais prejudiciais dos pesticidas químicos à saúde e
                                            ao meio ambiente.</p>
                                        <a class="action-ads-button large greennature-button"
                                            style="color: #ffffff;background-color: #5dc269;"
                                            href="{{ route('nav.mip') }}">Ver mais</a>
                                    </div>
                                </div>
                            </div>
                            <div class="clear"></div>
                        </div>
                    </div>
                    <div class="clear"></div>
                </section>

                <section id="content-section-5">
                    <div class="greennature-color-wrapper  gdlr-show-all greennature-skin-service-half greennature-half-bg-wrapper"
                        style="background-color: #f5f5f5; padding-bottom: 20px; ">
                        <div class="greennature-half-bg greennature-bg-solid"></div>
                        <div class="container">
                            <div class="six columns video-wrapper">
                                <iframe width="560" height="315"
                                    src="https://www.youtube.com/embed/{{ $informations->video }}" frameborder="0"
                                    allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
                                    allowfullscreen>
                                </iframe>
                            </div>
                            <div class="six columns">
                                <div class="greennature-item greennature-icon-with-list-item">
                                    <div class="list-with-icon-ux greennature-ux">
                                        <div class="list-with-icon greennature-left">
                                            <div class="list-with-icon-image">
                                                <img src="{{ asset('img/icon-1.png') }}" alt="" width="80"
                                                    height="80" />
                                            </div>
                                            <div class="list-with-icon-content">
                                                <div class="list-with-icon-title greennature-skin-title">Alta tecnologia
                                                </div>
                                                <div class="list-with-icon-caption">
                                                    <p>É alta tecnologia e qualificação para melhorar a produtividade no
                                                        campo, sem agredir o meio ambiente.</p>
                                                </div>
                                            </div>
                                            <div class="clear"></div>
                                        </div>
                                    </div>
                                    <div class="list-with-icon-ux greennature-ux">
                                        <div class="list-with-icon greennature-left">
                                            <div class="list-with-icon-image">
                                                <img src="{{ asset('img/icon-2.png') }}" alt="" width="80"
                                                    height="80" />
                                            </div>
                                            <div class="list-with-icon-content">
                                                <div class="list-with-icon-title greennature-skin-title">Pesquisas</div>
                                                <div class="list-with-icon-caption">
                                                    <p>Parceria com as maiores e mais conceituadas empresas de pesquisas
                                                        microbiológicas do Brasil.</p>
                                                </div>
                                            </div>
                                            <div class="clear"></div>
                                        </div>
                                    </div>
                                    <div class="list-with-icon-ux greennature-ux">
                                        <div class="list-with-icon greennature-left">
                                            <div class="list-with-icon-image">
                                                <img src="{{ asset('img/icon-3.png') }}" alt="" width="80"
                                                    height="80" />
                                            </div>
                                            <div class="list-with-icon-content">
                                                <div class="list-with-icon-title greennature-skin-title">Especialistas
                                                </div>
                                                <div class="list-with-icon-caption">
                                                    <p>Todos os processos são conduzidos por agrônomos e biólogos
                                                        especializados no assunto.</p>
                                                </div>
                                            </div>
                                            <div class="clear"></div>
                                        </div>
                                    </div>
                                    <div class="clear"></div>
                                </div>
                            </div>
                            <div class="clear"></div>
                        </div>
                    </div>
                    <div class="clear"></div>
                </section>

                <section id="content-section-4">
                    <div class="greennature-color-wrapper gdlr-show-all no-skin"
                        style="background-color: #ffffff; padding-bottom: 25px; ">
                        <div class="container">
                            <div
                                class="greennature-item-title-wrapper greennature-item  greennature-left-divider greennature-medium ">
                                <div class="greennature-item-title-container container">
                                    <div class="greennature-item-title-head">
                                        <h3
                                            class="greennature-item-title greennature-skin-title greennature-skin-border">
                                            <img src="{{ asset('img/icon-5.png') }}" alt="" width="80"
                                                height="80" />Bionews</h3>
                                        <a class="greennature-item-title-link" href="{{ route('nav.bionews') }}">Mais
                                            notícias</a>
                                        <div class="clear"></div>
                                    </div>
                                </div>
                            </div>
                            <div class="portfolio-item-wrapper type-classic-portfolio">
                                <div class="portfolio-item-holder  greennature-portfolio-column-4">
                                    <div class="greennature-isotope" data-type="portfolio" data-layout="fitRows">
                                        <div class="clear"></div>

                                        @foreach ($latestsNews as $post)

                                        <div class="three columns">
                                            <div
                                                class="greennature-item greennature-portfolio-item greennature-classic-portfolio">
                                                <div class="greennature-ux greennature-classic-portfolio-ux">
                                                    <a href="{{ route('nav.singlepost', ['url' => $post->url]) }}">
                                                        <div class="portfolio-thumbnail greennature-image">
                                                            <img src="{{ asset('img/bionews/'.$post->image) }}" alt=""
                                                                width="540" height="326" />
                                                            <span class="portfolio-overlay">&nbsp;</span>
                                                        </div>
                                                    </a>
                                                    <div class="portfolio-classic-content">
                                                        <h3 class="portfolio-title"><a
                                                                href="{{ route('nav.singlepost', ['url' => $post->url]) }}">{{ $post->title }}</a>
                                                        </h3>
                                                        <div class="greennature-portfolio-info">
                                                            <div class="clear"></div>
                                                        </div>
                                                        <div class="portfolio-excerpt">{{ $post->short_description }}
                                                            <div class="clear"></div><a
                                                                href="{{ route('nav.singlepost', ['url' => $post->url]) }}"
                                                                class="excerpt-read-more">Leia mais</a></div><a
                                                            class="portfolio-classic-learn-more" href="#">Learn More</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        @endforeach

                                    </div>
                                    <div class="clear"></div>
                                </div>
                            </div>
                            <div class="clear"></div>
                            <div class="clear"></div>
                        </div>
                    </div>
                    <div class="clear"></div>
                </section>

                <div class="container" style="margin-bottom:5rem;">
                    <div
                        class="greennature-item-title-wrapper greennature-item  greennature-left-divider greennature-medium ">
                        <div class="greennature-item-title-container container">
                            <div class="greennature-item-title-head">
                                <h3 class="greennature-item-title greennature-skin-title greennature-skin-border">
                                    <img src="{{ asset('img/icon-empresas.png') }}" alt="" width="80"
                                        height="80" />Empresas parceiras</h3>
                                <div class="clear"></div>
                            </div>
                        </div>
                    </div>

                    <p style="font-size: 16px; padding: 15px 28px; font-weight: 400;">
                        A Bionat trabalha em parceria com as maiores e mais conceituadas empresas de pesquisas microbiológicas do Brasil. 
                        Empresas conceituadas nacionalmente que permitem trazer produtos inéditos e inovadores ao mercado.
                    </p>

                    <div class="empresas">
                        <div><img src="{{ asset('img/croplife.jpg') }}" alt=""></div>
                        <div><img src="{{ asset('img/embrapa-logo.png') }}" alt=""></div>
                        <div><img src="{{ asset('img/instituto.jpeg') }}" alt=""></div>
                        <div><img src="{{ asset('img/esalq.jpeg') }}" alt=""></div>
                    </div>
                </div>
                <!-- Below Sidebar Section-->

            </div>
            <!-- greennature-content -->
            <div class="clear"></div>
        </div>
        <!-- content wrapper -->

    </div>
    @endsection