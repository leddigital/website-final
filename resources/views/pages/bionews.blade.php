@extends('layouts.site')
@section('title', 'Bionews - Bionat')
@section('page-title', (request()->get('s')!=""?"PROCURAR":"BIONEWS"))
@section('page-subtitle', (request()->get('s')!=""?"Você está buscando por: ".request()->get('s'):"Fique por dentro das
novidades do mundo da agrociência."))

@section('content')

@include('blocks.mainbanner')

<div class="body-wrapper float-menu">

    <!-- is search -->
    <div class="content-wrapper">
        <div class="greennature-content">

            <!-- Above Sidebar Section-->

            <!-- Sidebar With Content Section-->
            <div class="with-sidebar-wrapper">
                <div class="with-sidebar-container container">
                    <div class="with-sidebar-left twelve columns">
                        <div class="with-sidebar-content twelve columns">
                            <section id="content-section-1">
                                <div class="section-container container">
                                    <div class="blog-item-wrapper">
                                        <div class="blog-item-holder">
                                            <div class="greennature-isotope" data-type="blog" data-layout="fitRows">
                                                <div class="clear"></div>

                                                @if ($posts->total())

                                                @foreach ($posts as $post)

                                                <div class="four columns">
                                                    <div
                                                        class="greennature-item greennature-blog-grid greennature-skin-box">
                                                        <div class="greennature-ux greennature-blog-grid-ux">
                                                            <article id="post-852"
                                                                class="post-852 post type-post status-publish format-standard has-post-thumbnail hentry category-fit-row tag-blog tag-life-style">
                                                                <div class="greennature-standard-style">
                                                                    <div class="greennature-blog-thumbnail">
                                                                        <a
                                                                            href="{{ route('nav.singlepost', ['url' => $post->url]) }}">
                                                                            <img src="{{ asset('img/bionews/'.$post->feat_image) }}"
                                                                                alt="{{ $post->feat_image }}"
                                                                                width="400" height="300" />
                                                                        </a>
                                                                    </div>

                                                                    <div class="greennature-blog-grid-content">
                                                                        <header class="post-header">
                                                                            <h3 class="greennature-blog-title"><a
                                                                                    href="{{ route('nav.singlepost', ['url' => $post->url]) }}">{{ $post->title }}</a>
                                                                            </h3>

                                                                            <div class="greennature-blog-info">
                                                                                <div
                                                                                    class="blog-info blog-date greennature-skin-info">
                                                                                    <i class="fa fa-clock-o"></i><a
                                                                                        href=".">{{ date('d/m/Y', strtotime($post->created_at)) }}</a>
                                                                                </div>

                                                                                <div class="clear"></div>
                                                                            </div>
                                                                            <div class="clear"></div>
                                                                        </header>

                                                                        <div class="greennature-blog-content">
                                                                            {{  $post->short_description }}
                                                                            <div class="clear"></div><a
                                                                                href="{{ route('nav.singlepost', ['url' => $post->url]) }}"
                                                                                class="excerpt-read-more">Leia mais</a>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </article>
                                                            <!-- #post -->
                                                        </div>
                                                    </div>
                                                </div>

                                                @endforeach

                                                @else

                                                <div class="search-wrapper">

                                                    <div class="column eight">
                                                        <div class="blog-content-wrapper">
                                                            <header class="post-header">
                                                                <h3 class="greennature-blog-title">Não foram encontradas
                                                                    notícias para sua busca</h3>
                                                                <div class="clear"></div>
                                                            </header>
                                                            <div style="font-size:18px;" class="greennature-blog-content">
                                                                Tente faça uma nova pesquisa usando termos mais genéricos.
                                                            </div>
                                                        </div>

                                                        <div class="column six">
                                                            <div class="gdl-search-form">
                                                                <form method="GET" id="searchform"
                                                                    action="{{ route('nav.procurar') }}">
                                                                    <div class="search-text" id="search-text">
                                                                        <input type="text" name="s" id="s"
                                                                            autocomplete="off"
                                                                            placeholder="Pesquisar notícia..." />
                                                                    </div>
                                                                    <input type="submit" id="searchsubmit" value="" />
                                                                    <div class="clear"></div>
                                                                </form>
                                                            </div>
                                                        </div>

                                                    </div>

                                                    <div class="column four">
                                                        <img class="search-image" src="{{ asset('img/search.png') }}"
                                                            alt="procurar">
                                                    </div>

                                                </div>

                                                @endif

                                                <div class="clear"></div>
                                            </div>
                                        </div>

                                        {{ $posts->appends(request()->except('page'))->links() }}

                                        {{-- <div class="greennature-pagination"><span aria-current='page'
                                                class='page-numbers current'>1</span>
                                            <a class='page-numbers' href='page/2/index.html'>2</a>
                                            <a class="next page-numbers" href="page/2/index.html">Next &rsaquo;</a>
                                        </div> --}}
                                    </div>
                                    <div class="clear"></div>
                                </div>
                            </section>
                        </div>

                        <div class="clear"></div>
                    </div>

                </div>
            </div>

            <!-- Below Sidebar Section-->

        </div>
        <!-- greennature-content -->
        <div class="clear"></div>
    </div>
    <!-- content wrapper -->
</div>

@endsection
