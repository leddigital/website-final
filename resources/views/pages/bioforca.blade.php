@extends('layouts.site')
@section('title', 'Bioforças - Bionat')
@section('content')

<div class="body-wrapper float-menu">

    <div class="content-wrapper">
        <div class="greennature-content">

            <div class="with-sidebar-wrapper">
                <div class="with-sidebar-container container greennature-class-no-sidebar">
                    <div class="with-sidebar-left twelve columns">
                        <div class="with-sidebar-content twelve columns">
                            <div class="greennature-item greennature-portfolio-style2 greennature-item-start-content">
                                <div id="portfolio-76" class="post-76 portfolio type-portfolio status-publish has-post-thumbnail hentry portfolio_category-environment portfolio_category-volunteer portfolio_tag-donation portfolio_tag-volunteer">
                                    <div class="greennature-portfolio-thumbnail thumb-personalizada">
                                        <div class="greennature-stack-image-wrapper">
                                            <div class="greennature-stack-image">
                                                <a href="{{ route('nav.bioforca', ['url' => $bioforca->url]) }}" data-fancybox-group="greennature-gal-1" data-rel="fancybox">
                                                    <img src="{{ asset('img/bioforcas/'.$bioforca->image) }}" alt="" width="1280" height="853" />
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                                                   
                                    <div id="portfolio-content" class="greennature-portfolio-content">
                                        
                                        <div id="stick-port-inner">
                                        
                                        
                                        <div class="greennature-portfolio-info">
                                            <h4 class="head">Informações rápidas</h4>

                                            <div class="content">
                                                <div class="portfolio-info portfolio-clients">
                                                    <span class="info-head greennature-title">Nome </span>{{ $bioforca->title }}</div>
                                                
                                                @if (isset($bioforca->embalagem) && !empty($bioforca->embalagem))
                                                    <div class="portfolio-info portfolio-skills">
                                                        <span class="info-head greennature-title">Embalagem</span>
                                                        {{ $bioforca->embalagem }}
                                                    </div>
                                                @endif

                                                @if (isset($bioforca->categoria) && !empty($bioforca->categoria))
                                                    <div class="portfolio-info portfolio-skills">
                                                        <span class="info-head greennature-title">Categoria</span>
                                                        {{ $bioforca->categoria }}
                                                    </div>
                                                @endif
                                                
                                                @if (isset($bioforca->concentracao) && !empty($bioforca->concentracao))
                                                    <div class="portfolio-info portfolio-skills">
                                                        <span class="info-head greennature-title">Concentração</span>
                                                        {{ $bioforca->concentracao }}
                                                    </div>
                                                @endif

                                                @if (isset($bioforca->composicao) && !empty($bioforca->composicao))
                                                    <div class="portfolio-info portfolio-skills">
                                                        <span class="info-head greennature-title">Principal composto</span>
                                                        {{ $bioforca->composicao }}
                                                    </div>
                                                @endif

                                                <div class="clear"></div>

                                            </div>
                                        </div>

                                        <a class="action-ads-button large greennature-button bioforca-button" 
                                            href="{{ asset('img/bioforcas/bulas/sample.pdf') }}"
                                            style="color: #6d5b1c;background-color: #fec428;" download>
                                            <i class="fa fa-download" aria-hidden="true"></i> Baixar Bula</a>

                                    </div>

                                    </div>

                                </div>

                                <div class="clear"></div>

                                <div class="six columns">
                                    <article class="bio-descricao">
                                        <div class="greennature-item greennature-accordion-item style-1" style="margin-bottom: 60px;">
                                            
                                            @if (isset($bioforca->short_description) && !empty($bioforca->short_description))
                                                <div class="accordion-tab active pre-active">
                                                    <h4 class="accordion-title"><i class="icon-minus" ></i><span>Descritivo</span></h4>
                                                    <div class="accordion-content">
                                                        {!! $bioforca->short_description !!}
                                                    </div>
                                                </div>
                                            @endif
                                            
                                            @if (isset($bioforca->desc_recomendacao) && !empty($bioforca->desc_recomendacao))
                                            <div class="accordion-tab">
                                                <h4 class="accordion-title"><i class="icon-plus" ></i><span>Recomendação</span></h4>
                                                <div class="accordion-content">
                                                    {!! $bioforca->desc_recomendacao !!}
                                                </div>
                                            </div>
                                            @endif

                                            @if (isset($bioforca->desc_composicao) && !empty($bioforca->desc_composicao))
                                            <div class="accordion-tab">
                                                <h4 class="accordion-title"><i class="icon-plus" ></i><span>Composição</span></h4>
                                                <div class="accordion-content">
                                                    {!! $bioforca->desc_composicao !!}
                                                </div>
                                            </div>
                                            @endif

                                            @if (isset($bioforca->desc_cultura) && !empty($bioforca->desc_cultura))
                                            <div class="accordion-tab">
                                                <h4 class="accordion-title"><i class="icon-plus" ></i><span>Culturas recomendadas</span></h4>
                                                <div class="accordion-content">
                                                    {!! $bioforca->desc_cultura !!}
                                                </div>
                                            </div>
                                            @endif

                                            @if (isset($bioforca->sdesc_quando) && !empty($bioforca->desc_quando))
                                            <div class="accordion-tab">
                                                <h4 class="accordion-title"><i class="icon-plus" ></i><span>Quando utilizar</span></h4>
                                                <div class="accordion-content">
                                                    {!! $bioforca->desc_quando !!}
            
                                                </div>
                                            </div>
                                            @endif

                                            @if (isset($bioforca->desc_funciona) && !empty($bioforca->desc_funciona))
                                            <div class="accordion-tab">
                                                <h4 class="accordion-title"><i class="icon-plus" ></i><span>Como funciona</span></h4>
                                                <div class="accordion-content">
                                                   {!! $bioforca->desc_funciona !!}
                                                </div>
                                            </div>
                                            @endif

                                            @if (isset($bioforca->desc_aplicar) && !empty($bioforca->desc_aplicar))
                                            <div class="accordion-tab">
                                                <h4 class="accordion-title"><i class="icon-plus" ></i><span>Como aplicar</span></h4>
                                                <div class="accordion-content">
                                                   {!! $bioforca->desc_aplicar !!}
                                                </div>
                                            </div>
                                            @endif

                                            @if (isset($bioforca->desc_compatibilidade) && !empty($bioforca->desc_compatibilidade))
                                            <div class="accordion-tab">
                                                <h4 class="accordion-title"><i class="icon-plus" ></i><span>Compatibilidade</span></h4>
                                                <div class="accordion-content">
                                                   {!! $bioforca->desc_compatibilidade !!}
                                                </div>
                                            </div>
                                            @endif

                                            @if (isset($bioforca->desc_armazenamento) && !empty($bioforca->desc_armazenamento))
                                            <div class="accordion-tab">
                                                <h4 class="accordion-title"><i class="icon-plus" ></i><span>Armazenamento</span></h4>
                                                <div class="accordion-content">
                                                   {!! $bioforca->desc_armazenamento !!}
                                                </div>
                                            </div>
                                            @endif

                                            @if (isset($bioforca->desc_transporte) && !empty($bioforca->desc_transporte))
                                            <div class="accordion-tab">
                                                <h4 class="accordion-title"><i class="icon-plus" ></i><span>Transporte</span></h4>
                                                <div class="accordion-content">
                                                   {!! $bioforca->desc_transporte !!}
                                                </div>
                                            </div>
                                            @endif

                                        </div>
                                    </article>
                                </div>

                            </div>

                        </div>

                        <div class="clear"></div>
                    </div>

                    <div class="clear"></div>
                </div>
            </div>

        </div>
        <!-- greennature-content -->
        <div class="clear"></div>
    </div>
    <!-- content wrapper -->

@endsection
