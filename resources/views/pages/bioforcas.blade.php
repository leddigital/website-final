@extends('layouts.site')
@section('title', 'Bioforças - Bionat')
@section('page-title', 'BIOFORÇAS')
@section('page-subtitle', 'Conheça as Soluções Biológicas da Bionat')
@section('content')

@include('blocks.mainbanner')

<div class="body-wrapper float-menu">

<!-- is search -->
<div class="content-wrapper">
    <div class="greennature-content">

        <!-- Above Sidebar Section-->

        <!-- Sidebar With Content Section-->
        <div class="with-sidebar-wrapper">
            <section id="content-section-1">
                <div class="section-container container">
                    <div class="portfolio-item-wrapper type-classic-portfolio" style="margin-bottom: 20px;">
                        <div class="portfolio-item-holder  greennature-portfolio-column-3">
                            <div class="greennature-isotope" data-type="portfolio" data-layout="fitRows">
                                <div class="clear"></div>

                                @foreach ($bioforcas as $bioforca)

                                <div class="four columns bioforca-card">
                                    <div
                                        class="greennature-item greennature-portfolio-item greennature-classic-portfolio">
                                        <div class="greennature-ux greennature-classic-portfolio-ux">
                                            <div class="portfolio-thumbnail greennature-image">
                                                <img src="{{ asset('img/bioforcas/'.$bioforca->image) }}" alt="" width="400" height="300" />
                                                <span class="portfolio-overlay">&nbsp;</span>
                                                <a class="portfolio-overlay-icon" href="{{ route('nav.bioforca', ['url' => $bioforca->url]) }}">
                                                    <span class="portfolio-icon"><i class="fa fa-link"></i></span>
                                                </a>
                                            </div>
                                            <div class="portfolio-classic-content">
                                                <h3 class="portfolio-title">
                                                    <a href="{{ route('nav.bioforca', ['url' => $bioforca->url]) }}">{{ $bioforca->title }}</a>
                                                </h3>
                                                <div class="portfolio-excerpt custom-text">
                                                        {{ $bioforca->short_description }}
                                                    <div class="clear"></div>
                                                    {{-- <a href="{{ route('nav.bioforca', ['url' => $bioforca->url]) }}" class="excerpt-read-more">Ver mais</a> --}}
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                </div>

                                @endforeach

                            </div>
                            <div class="clear"></div>
                        </div>
                    </div>
                    <div class="clear"></div>
                </div>
            </section>
        </div>
        <!-- Below Sidebar Section-->

    </div>
    <!-- greennature-content -->
    <div class="clear"></div>
</div>
<!-- content wrapper -->

</div>

@endsection
