@extends('layouts.site')
@section('title', 'Sobre a Bionat')
@section('page-title', 'BIONAT')
@section('page-subtitle', 'Conheça um pouco da nossa história.')
@section('content')

<div class="body-wrapper float-menu">

    <!-- is search -->
    <div class="content-wrapper">
        <div class="greennature-content">

            <!-- Above Sidebar Section-->

            <!-- Sidebar With Content Section-->
            <div class="with-sidebar-wrapper">

                @include('blocks.mainbanner')

                <section id="content-section-2">
                    <div class="greennature-color-wrapper  gdlr-show-all no-skin greennature-half-bg-wrapper" style="background-color: #ffffff; ">
                        <div class="greennature-half-bg greennature-bg-solid custom-bg" style="background-image: url('{{ asset('img/bionat.jpg') }}');"></div>
                        <div class="container">
                            <div class="six columns"></div>
                            <div class="six columns">
                                <div class="greennature-item greennature-about-us-item greennature-normal">
                                    <div class="about-us-title-wrapper">
                                        <h3 class="about-us-title">GERAR SOLUÇÕES BIOLÓGICAS NATURAIS, SUSTENTÁVEIS, INTELIGENTES E EFICIENTES PARA O AGRO.</h3>
                                        {{-- <div class="about-us-caption greennature-title-font greennature-skin-info">Amet Dapibus Mollis</div> --}}
                                        <div class="about-us-title-divider"></div>
                                    </div>
                                    <div class="about-us-content-wrapper">
                                        <div class="about-us-content greennature-skin-content custom-text">
                                            <p> Nossa estrutura fabril usa tecnologia de ponta associada a processos produtivos rigorosos,
                                                rastreáveis, eficientes e específicos para a produção de agentes biológicos de alta qualidade,
                                                reconhecidos por vários cientistas da área.
                                            </p>
                                            <p> A Bionat trabalha em parceria com as maiores e mais conceituadas empresas de pesquisas microbiológicas do Brasil,
                                                para trazer produtos inéditos e inovadores ao mercado. Trabalhamos fortemente com a tecnologia de aplicação,
                                                que busca potencializar nossas soluções.
                                            </p>
                                            <p>Todos os processos são conduzidos por agrônomos e biólogos especializados no assunto.</p>
                                            <p>É alta tecnologia e qualificação para melhorar a produtividade no campo, sem agredir o meio ambiente</p>
                                        </div>
                                        {{-- <a class="about-us-read-more greennature-button" href="#">Read More</a> --}}
                                    </div>
                                    <div class="clear"></div>
                                </div>
                            </div>
                            <div class="clear"></div>
                        </div>
                    </div>
                    <div class="clear"></div>
                </section>

                <!-- Section Crop Life -->
                <section id="content-section-3">
                    <div class="greennature-color-wrapper  gdlr-show-all no-skin" style="background-color: #f3f3f3; padding-top: 70px; padding-bottom: 53px; ">
                        <div class="container">
                            <div class="four columns">
                                <img style="margin: auto;display: block;" src="{{ asset('img/Croplife-Logo.png') }}" alt="CropLife">
                            </div>
                            <div class="eight columns crop-text">
                                <div class="greennature-item greennature-about-us-item greennature-normal">
                                    <div class="about-us-title-wrapper">
                                        {{-- <h3 class="about-us-title">Risus Ultricies Fringilla Fusce</h3> --}}
                                        {{-- <div class="about-us-caption greennature-title-font greennature-skin-info">Amet Dapibus Mollis</div> --}}
                                        <div class="about-us-title-divider"></div>
                                    </div>
                                    <div class="about-us-content-wrapper">
                                        <div class="about-us-content greennature-skin-content">
                                            <p>A Bionat é associada CropLife. A CropLife Brasil tem como missão fomentar a inovação e o uso das mais modernas tecnologias nas lavouras brasileiras, apoiar iniciativas de educação e treinamento no campo, além de estreitar o diálogo com a sociedade.</p>
                                        </div>
                                    </div>
                                    <div class="clear"></div>
                                </div>
                            </div>
                            <div class="clear"></div>
                        </div>
                    </div>
                    <div class="clear"></div>
                </section>

                <section id="content-section-5">
                    <div class="greennature-color-wrapper  gdlr-show-all no-skin" style="background-color: #ffffff; padding-top: 95px; padding-bottom: 5px; ">
                        <div class="container">
                            <div class="greennature-testimonial-item-wrapper" style="margin-bottom: 80px;">
                                <div class="greennature-item-title-wrapper greennature-item  greennature-nav-container greennature-left greennature-medium ">
                                    <div class="greennature-item-title-container container">
                                        <div class="greennature-item-title-head">
                                            <h3 class="greennature-item-title greennature-skin-title greennature-skin-border">Conheça a Bionat</h3><span class="greennature-nav-title"><i class="icon-angle-left greennature-flex-prev"></i><i class="icon-angle-right greennature-flex-next"></i></span>
                                            <div class="clear"></div>
                                        </div>
                                    </div>
                                </div>
                                <div class="greennature-item greennature-testimonial-item carousel plain-style">
                                    <div class="greennature-ux greennature-testimonial-ux">
                                        <div class="flexslider" data-type="carousel" data-nav-container="greennature-testimonial-item" data-columns="3">
                                            <ul class="slides">
                                                <li class="testimonial-item">
                                                    <div class="testimonial-content-wrapper">
                                                        <div class="testimonial-content greennature-skin-content">
                                                            <img src="{{ asset('img/galeria/B1.jpg') }}" alt="">
                                                        </div>
                                                        {{-- <div class="testimonial-info"><span class="testimonial-author greennature-skin-link-color">John Doe<span>, </span></span><span class="testimonial-position greennature-skin-info">Head Cheff</span></div> --}}
                                                    </div>
                                                </li>
                                                <li class="testimonial-item">
                                                    <div class="testimonial-content-wrapper">
                                                        <div class="testimonial-content greennature-skin-content">
                                                            <img src="{{ asset('img/galeria/B2.jpg') }}" alt="">
                                                        </div>
                                                        {{-- <div class="testimonial-info"><span class="testimonial-author greennature-skin-link-color">Ricardo Goff<span>, </span></span><span class="testimonial-position greennature-skin-info">Lawyer</span></div> --}}
                                                    </div>
                                                </li>
                                                <li class="testimonial-item">
                                                    <div class="testimonial-content-wrapper">
                                                        <div class="testimonial-content greennature-skin-content">
                                                            <img src="{{ asset('img/galeria/B3.jpg') }}" alt="">
                                                        </div>
                                                        {{-- <div class="testimonial-info"><span class="testimonial-author greennature-skin-link-color">Jennifer Dawn<span>, </span></span><span class="testimonial-position greennature-skin-info">Dentist</span></div> --}}
                                                    </div>
                                                </li>
                                                <li class="testimonial-item">
                                                    <div class="testimonial-content-wrapper">
                                                        <div class="testimonial-content greennature-skin-content">
                                                            <img src="{{ asset('img/galeria/B4.jpg') }}" alt="">
                                                        </div>
                                                        {{-- <div class="testimonial-info"><span class="testimonial-author greennature-skin-link-color">Paul Smith<span>, </span></span><span class="testimonial-position greennature-skin-info">Doctor</span></div> --}}
                                                    </div>
                                                </li>
                                                <li class="testimonial-item">
                                                    <div class="testimonial-content-wrapper">
                                                        <div class="testimonial-content greennature-skin-content">
                                                            <img src="{{ asset('img/galeria/B5.jpg') }}" alt="">
                                                        </div>
                                                        {{-- <div class="testimonial-info"><span class="testimonial-author greennature-skin-link-color">Alan Christier<span>, </span></span><span class="testimonial-position greennature-skin-info">Accountant</span></div> --}}
                                                    </div>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="clear"></div>
                            <div class="clear"></div>
                        </div>
                    </div>
                    <div class="clear"></div>
                </section>

                {{-- <section class="section-container container" style="margin-bottom:5rem;">

                    <div class="fade">
                        <div><img src="{{ asset('img/galeria/B1.jpg') }}" alt=""></div>
                        <div><img src="{{ asset('img/galeria/B2.jpg') }}" alt=""></div>
                        <div><img src="{{ asset('img/galeria/B3.jpg') }}" alt=""></div>
                        <div><img src="{{ asset('img/galeria/B4.jpg') }}" alt=""></div>
                        <div><img src="{{ asset('img/galeria/B5.jpg') }}" alt=""></div>
                    </div>

                </section> --}}

                {{-- @include('blocks.bioforcas') --}}

            </div>
            <!-- Below Sidebar Section-->

        </div>
        <!-- greennature-content -->
        <div class="clear"></div>
    </div>
    <!-- content wrapper -->

</div>

@endsection
