@extends('layouts.site')
@section('title', 'MIP - Bionat')
@section('page-title', 'MIP')
@section('page-subtitle', 'Manejo Integrado de Pragas')
@section('content')

<div class="body-wrapper float-menu">

    <!-- is search -->
    <div class="content-wrapper">
        <div class="greennature-content">

            <!-- Above Sidebar Section-->

            <!-- Sidebar With Content Section-->
            <div class="with-sidebar-wrapper">

                @include('blocks.mainbanner')

                <section id="content-section-2">
                    <div class="greennature-color-wrapper  gdlr-show-all no-skin greennature-half-bg-wrapper" style="background-color: #ffffff; ">
                        <div class="greennature-half-bg greennature-bg-solid custom-bg" style="background-image: url('{{ asset('img/mip.jpg') }}');"></div>
                        <div class="container">
                            <div class="six columns"></div>
                            <div class="six columns">
                                <div class="greennature-item greennature-about-us-item greennature-normal">
                                    <div class="about-us-title-wrapper">
                                        <h3 class="about-us-title">MIP é manejo integrado de pragas e doenças.</h3>
                                        {{-- <div class="about-us-caption greennature-title-font greennature-skin-info">Amet Dapibus Mollis</div> --}}
                                        <div class="about-us-title-divider"></div>
                                    </div>
                                    <div class="about-us-content-wrapper">
                                        <div class="about-us-content greennature-skin-content custom-text">
                                            <p>
                                                O Manejo Integrado de Pragas (MIP) é uma técnica de controle ecológico. Esta prática procura 
                                                manter sempre baixo os níveis de pragas prejudiciais as lavouras. O controle é feito de forma 
                                                que interfira minimamente no campo e diminua as chances dos insetos ou doenças de reagirem e se 
                                                adaptarem a qualquer prática de defesa aplicada. O MIP é uma alternativa que diminui o uso de 
                                                agroquímicos, responsáveis por fortalecer a resistência das pragas, e reduz a contaminação de 
                                                alimentos e meio ambiente. 
                                            </p>
                                            <p>
                                                O Manejo Integrado de Pragas e Doenças não elimina definitivamente os agentes. Ele trabalha na redução de pragas 
                                                para manter o equilíbrio no ecossistema, tornando natural a relação de vivência entre ambiente, 
                                                plantação e insetos agressores, sem prejudicar qualquer parte do meio. 
                                            </p>
                                        {{-- <a class="about-us-read-more greennature-button" href="#">Read More</a> --}}
                                    </div>
                                    <div class="clear"></div>
                                </div>
                            </div>
                            <div class="clear"></div>
                        </div>
                    </div>
                    <div class="clear"></div>
                </section>

                {{-- @include('blocks.bioforcas') --}}

            </div>
            <!-- Below Sidebar Section-->

        </div>
        <!-- greennature-content -->
        <div class="clear"></div>
    </div>
    <!-- content wrapper -->

</div>

@endsection
