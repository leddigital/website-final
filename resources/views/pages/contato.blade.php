@extends('layouts.site')
@section('title', 'Contato - Bionat')
@section('content')

<div class="body-wrapper float-menu">

    <!-- is search -->

    <div class="content-wrapper">
        <div class="greennature-content">

            <!-- Sidebar With Content Section-->
            <div class="with-sidebar-wrapper">
                <div class="with-sidebar-container container">
                    <div class="with-sidebar-left eight columns">
                        <div class="with-sidebar-content twelve columns">
                            <section id="content-section-2">
                                <div class="section-container container">
                                    <div class="greennature-item greennature-content-item" style="margin-bottom: 60px;">
                                        <span class="clear"></span>
                                        <span class="greennature-space"
                                            style="margin-top: -22px; display: block;"></span>

                                        <h5 class="greennature-heading-shortcode" style="font-weight: bold;">
                                            Deixe sua mensagem, crítica ou sugestão que retornaremos assim que possível.
                                        </h5>

                                        <p><span class="clear"></span><span class="greennature-space"
                                                style="margin-top: 25px; display: block;"></span>
                                            <div role="form" class="wpcf7" id="wpcf7-f4-o1" lang="en-US" dir="ltr">

                                                <form id="contact-form" class="quform" action="{{ route('send.mail') }}"
                                                    method="POST">

                                                    @csrf

                                                    <div class="quform-elements">
                                                        <div class="quform-element">
                                                            <p>
                                                                <span class="wpcf7-form-control-wrap your-name">
                                                                    <input id="name" type="text" name="nome" size="40"
                                                                        class="input1" aria-required="true"
                                                                        aria-invalid="false" placeholder="Nome*" required>
                                                                </span>
                                                            </p>
                                                        </div>
                                                        <div class="quform-element">
                                                            <p>
                                                                <span class="wpcf7-form-control-wrap your-name">
                                                                    <input id="empresa" type="text" name="empresa"
                                                                        size="40" class="input1" aria-required="true"
                                                                        aria-invalid="false" placeholder="Empresa*">
                                                                </span>
                                                            </p>
                                                        </div>
                                                        <div class="quform-element">
                                                            <p>
                                                                <span class="wpcf7-form-control-wrap your-email">
                                                                    <input id="email" type="email" name="email" size="40"
                                                                        class="input1" aria-required="true"
                                                                        aria-invalid="false" placeholder="E-mail*" required>
                                                                </span>
                                                            </p>
                                                        </div>
                                                        <div class="quform-element">
                                                            <p>
                                                                <span class="wpcf7-form-control-wrap your-email">
                                                                    <input id="telefone" type="text" name="telefone"
                                                                        size="40" class="input1" aria-required="true"
                                                                        aria-invalid="false" placeholder="Telefone*"
                                                                        pattern="\([0-9]{2}\)[\s][0-9]{4}-[0-9]{4,5}" required>
                                                                </span>
                                                            </p>
                                                        </div>
                                                        <div class="quform-element">
                                                            <p>
                                                                <span class="wpcf7-form-control-wrap your-message">
                                                                    <textarea id="message" name="mensagem" cols="40"
                                                                        rows="10" class="input1" aria-invalid="false"
                                                                        placeholder="Sua mensagem*" required></textarea>
                                                                </span>
                                                            </p>
                                                        </div>
                                                        <p>
                                                            <!-- Begin Submit button -->
                                                            <div class="quform-submit">
                                                                <div class="quform-submit-inner">
                                                                    <button type="submit"
                                                                        class="submit-button"><span>Enviar</span></button>
                                                                </div>

                                                                <div style="margin-top:1rem;" class="quform-loading-wrap">
                                                                    <img id="gif-contato" style="height: 40px; display:none;" src="{{ asset('img/loading.gif') }}" alt="">
                                                                    <h5 id="status-msg" class="greennature-heading-shortcode"></h5>
                                                                </div>

                                                            </div>
                                                        </p>
                                                    </div>
                                                </form>
                                            </div>
                                        </p>
                                    </div>
                                    <div class="clear"></div>
                                </div>
                            </section>
                        </div>

                        <div class="clear"></div>
                    </div>

                    <div class="greennature-sidebar greennature-right-sidebar four columns">
                        <div class="greennature-item-start-content sidebar-right-item">
                            <div id="text-6" class="widget widget_text greennature-item greennature-widget">
                                <h3 class="greennature-widget-title">Trabalhe conosco</h3>
                                <div class="clear"></div>
                                <div class="textwidget">
                                    Envie seu currículo para <br>
                                    <a href="mailto:{{ $informations->email }}">{{ $informations->email }}</a>
                                </div>
                            </div>
                            <div id="text-7" class="widget widget_text greennature-item greennature-widget">
                                <h3 class="greennature-widget-title">Informações de contato</h3>
                                <div class="clear"></div>
                                <div class="textwidget">
                                    <p>{{ $informations->address }}, {{ $informations->number }} <br>
                                        {{ $informations->complement }} <br>
                                        {{ $informations->district }} - CEP {{ $informations->zipcode }} <br>
                                        {{ $informations->city }}/{{ $informations->state }}</p>
                                    <p><i class="greennature-icon fa fa-phone"
                                            style="vertical-align: middle; color: #444444; font-size: 16px; "></i>
                                        <a
                                            href="tel:+55<?= preg_replace('/[^0-9]+/','', $informations->phone1)?>">{{ $informations->phone1 }}</a>
                                    </p>
                                    <p><i class="greennature-icon fa fa-envelope"
                                            style="vertical-align: middle; color: #444444; font-size: 16px; "></i>
                                        <a href="mailto:{{ $informations->email }}">{{ $informations->email }}</a></p>
                                    <p><i class="greennature-icon fa fa-clock-o"
                                            style="vertical-align: middle; color: #444444; font-size: 16px; "></i>
                                        {{ $informations->opening_hours }}</p>
                                </div>
                            </div>
                            {{-- <div id="text-8" class="widget widget_text greennature-item greennature-widget">
                                <h3 class="greennature-widget-title">Mídias Sociais</h3>
                                <div class="clear"></div>
                                <div class="textwidget"><a href="http://facebook.com/goodlayers"><i
                                            class="greennature-icon fa fa-facebook"
                                            style="vertical-align: middle; color: #444444; font-size: 28px; "></i></a>
                                    <a href="http://twitter.com/goodlayers"><i class="greennature-icon fa fa-twitter"
                                            style="vertical-align: middle; color: #444444; font-size: 28px; "></i></a>
                                    <a href="#"><i class="greennature-icon fa fa-dribbble"
                                            style="vertical-align: middle; color: #444444; font-size: 28px; "></i></a>
                                    <a href="#"><i class="greennature-icon fa fa-pinterest"
                                            style="vertical-align: middle; color: #444444; font-size: 28px; "></i></a>
                                    <a href="#"><i class="greennature-icon fa fa-google-plus"
                                            style="vertical-align: middle; color: #444444; font-size: 28px; "></i></a>
                                    <a href="#"><i class="greennature-icon fa fa-instagram"
                                            style="vertical-align: middle; color: #444444; font-size: 28px; "></i></a>
                                </div>
                            </div> --}}
                        </div>
                    </div>
                    <div class="clear"></div>
                </div>
            </div>

            {{-- <!-- Above Sidebar Section-->
              <div class="above-sidebar-wrapper">
                <section id="content-section-1">
                    <div class="greennature-full-size-wrapper gdlr-show-all no-skin"
                        style="padding-bottom: 0px;  background-color: #ffffff; ">
                        <div class="greennature-item greennature-content-item" style="margin-bottom: 0px;">
                            <iframe
                                src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d7463.657970962218!2d-49.002255241049696!3d-20.71716796217807!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x94bc68866921ac91%3A0xe72b68e3d478a035!2sKimberlit%20agroci%C3%AAncias!5e0!3m2!1spt-BR!2sbr!4v1576510820466!5m2!1spt-BR!2sbr"
                                width="100%" height="480" frameborder="0" style="border:0;" allowfullscreen=""></iframe>
                            <div
                                style="position: absolute;width: 80%;bottom: 20px;left: 0;right: 0;margin-left: auto;margin-right: auto;color: #000;">
                            </div>
                            <div class="clear"></div>
                            <div class="clear"></div>
                        </div>
                        <div class="clear"></div>
                </section>
            </div> --}}

        </div>
        <!-- greennature-content -->
        <div class="clear"></div>
    </div>
    <!-- content wrapper -->



    <div class="modal" data-modal>
        <div class="modal-content">
            <button role="button" class="close-icon" data-modal="close-modal">X</button>
            <div class="modal-header">
                <h2 class="modal-title">Title</h2>
            </div>
            <div class="modal-body">
                <p>Modal Content</p>
            </div>
            <div class="modal-footer">
                <button role="button" class="close-button" data-modal="close-modal">Fechar</button>
            </div>
        </div>
    </div>

    @endsection
