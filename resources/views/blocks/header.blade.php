<header class="greennature-header-wrapper header-style-5-wrapper greennature-header-with-top-bar">
    <!-- top navigation -->
    
    <!-- <div class="top-navigation-wrapper">
        <div class="top-navigation-container container">
            <div class="top-navigation-left">
                <div class="top-navigation-left">
                    <div class="top-navigation-left-text">
                        <ul class="d-inline">
                            <li class="d-inline-item mr-2">Fone : <a
                                    href="tel:<?= preg_replace('/[^0-9]+/','', $informations->phone1)?>">{{ $informations->phone1 }}</a>
                            </li>
                            <li class="d-inline-item mr-2">E-mail : <a href="mailto:{{ $informations->email }}"
                                    target="_blank">{{ $informations->email }}</a></li>
                            <li class="d-inline-item mr-2"><a href="{{ route('nav.biblionat') }}">Biblionat</a></li>
                            <li class="d-inline-item mr-2"><a href="{{ route('nav.calculonat') }}">Calculonat</a></li>
                        </ul>
                    </div>
                </div>
            </div>

            <div class="top-navigation-right">
                <div class="top-social-wrapper">
                    <div class="social-icon">
                        <a href="#" target="_blank">
                            <i class="fa fa-facebook"></i></a>
                    </div>
                    <div class="social-icon">
                        <a href="#" target="_blank">
                            <i class="fa fa-flickr"></i></a>
                    </div>
                    <div class="social-icon">
                        <a href="#" target="_blank">
                            <i class="fa fa-linkedin"></i></a>
                    </div>
                    <div class="social-icon">
                        <a href="#" target="_blank">
                            <i class="fa fa-pinterest-p"></i></a>
                    </div>
                    <div class="social-icon">
                        <a href="#" target="_blank">
                            <i class="fa fa-tumblr"></i></a>
                    </div>
                    <div class="social-icon">
                        <a href="#" target="_blank">
                            <i class="fa fa-twitter"></i></a>
                    </div>
                    <div class="social-icon">
                        <a href="#" target="_blank">
                            <i class="fa fa-vimeo"></i></a>
                    </div>
                    <div class="clear"></div>
                </div>
            </div>

            <div class="clear"></div>
        </div>
    </div> -->
    
    <div id="greennature-header-substitute"></div>
    <div class="greennature-header-inner header-inner-header-style-5">
        <div class="greennature-header-container container">
            <div class="greennature-header-inner-overlay"></div>
            <!-- logo -->
            <div class="greennature-logo">
                <div class="greennature-logo-inner">
                    <a href="{{ route('nav.index') }}">
                        <img src="{{ asset('img/logo.png') }}" alt="" />
                    </a>
                </div>
                <div class="greennature-responsive-navigation dl-menuwrapper" id="greennature-responsive-navigation">
                    <button class="dl-trigger">Open Menu</button>
                    <ul id="menu-main-menu" class="dl-menu greennature-main-mobile-menu">
                        <li class="menu-item menu-item-home greennature-normal-menu {{ Request::is('')?"current-menu-item":"" }}"><a href="{{ route('nav.index') }}">Home</a></li>
                        <li class="menu-item menu-item-home greennature-normal-menu {{ Request::is('bionat')?"current-menu-item":"" }}"><a href="{{ route('nav.bionat') }}">Bionat</a></li>
                        <li class="menu-item menu-item-home greennature-normal-menu {{ Request::is('bioforcas')?"current-menu-item":"" }}"><a href="{{ route('nav.bioforcas') }}">Bioforças</a></li>
                        <li class="menu-item menu-item-home greennature-normal-menu {{ Request::is('mip')?"current-menu-item":"" }}"><a href="{{ route('nav.mip') }}">MIP</a></li>
                        <li class="menu-item menu-item-home greennature-normal-menu {{ Request::is('bionews')?"current-menu-item":"" }}"><a href="{{ route('nav.bionews') }}">Bionews</a></li>
                        <li class="menu-item menu-item-home greennature-normal-menu {{ Request::is('equipe-comercial')?"current-menu-item":"" }}"><a href="{{ route('nav.equipe-comercial') }}">Equipe Comercial</a></li>
                        <li class="menu-item menu-item-home greennature-normal-menu"><a href="#">Contato</a></li>
                    </ul>
                </div>
            </div>

            <!-- navigation -->
            <div class="greennature-navigation-wrapper">
                <nav class="greennature-navigation" id="greennature-main-navigation">
                    <ul id="menu-main-menu-1" class="sf-menu greennature-main-menu">
                        <li class="menu-item menu-item-home greennature-normal-menu {{ Request::is('/')?"current-menu-item":"" }}"><a href="{{ route('nav.index') }}">Home</a></li>
                        <li class="menu-item menu-item-home greennature-normal-menu {{ Request::is('bionat')?"current-menu-item":"" }}"><a href="{{ route('nav.bionat') }}">Bionat</a></li>
                        <li class="menu-item menu-item-home greennature-normal-menu {{ Request::is('bioforcas')?"current-menu-item":"" }}"><a href="{{ route('nav.bioforcas') }}">Bioforças</a></li>
                        <li class="menu-item menu-item-home greennature-normal-menu {{ Request::is('mip')?"current-menu-item":"" }}"><a href="{{ route('nav.mip') }}">MIP</a></li>
                        <li class="menu-item menu-item-home greennature-normal-menu {{ Request::is('bionews')?"current-menu-item":"" }}"><a href="{{ route('nav.bionews') }}">Bionews</a></li>
                        <li class="menu-item menu-item-home greennature-normal-menu {{ Request::is('equipe-comercial')?"current-menu-item":"" }}"><a href="{{ route('nav.equipe-comercial') }}">Equipe Comercial</a></li>
                    </ul>
                    <a href="{{ route('nav.contato') }}" class="greennature-donate-button "><span
                            class="greennature-button-overlay">
                        </span><span class="greennature-button-donate-text">Contato</span>
                    </a>
                </nav>
                <div class="greennature-navigation-gimmick" id="greennature-navigation-gimmick"></div>
                <div class="clear"></div>
            </div>
            <div class="clear"></div>
        </div>
    </div>
</header>
