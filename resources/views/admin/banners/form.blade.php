@extends('layouts.admin')
@section('title', 'Banners')
@section('content')
<header class="page-header">
    <h2>Banners</h2>
    <div class="right-wrapper text-right">
        <ol class="breadcrumbs">
            <li>
                <a href="{{ route('information.index') }}">
                    <i class="fas fa-home"></i>
                </a>
            </li>
            <li><span>Cadastro de Banners</span></li>
        </ol>
        <a class="sidebar-right-toggle" data-open=""><i class="fas fa-chevron-left"></i></a>
    </div>
</header>
<div class="row">
    <div class="col">
        <form class="form-horizontal form-bordered" id="form_cadastre" method="post"
            action="{{ isset($entity->id)?route('banners.edit.save',['id'=>$entity->id]):route('banners.save') }}"
            data-reload="{{ route('banners.index') }}">
            @csrf

            <section class="card">
                <header class="card-header">
                    <div class="card-actions">
                        <a href="forms-basic.html#" class="card-action card-action-toggle" data-card-toggle=""></a>
                    </div>

                    <h2 class="card-title">Título</h2>
                    <p class="card-subtitle">
                        Informe o título do banner.
                    </p>
                </header>
                <div class="card-body" style="display: block;">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="form-group row">
                                <div class="col-lg-12">
                                    <div class="input-group">
                                        <span class="input-group-prepend">
                                            <span class="input-group-text">
                                                <i class="fas fa-align-left"></i>
                                            </span>
                                        </span>
                                        <input value="{{ isset($entity->title)!=""?$entity->title:'' }}" type="text"
                                            name="title" class="form-control" placeholder="Título do banner" required>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row">
                            </div>
                        </div>
                    </div>
                </div>
            </section>

            <section class="card">
                <header class="card-header">
                    <div class="card-actions">
                        <a href="forms-basic.html#" class="card-action card-action-toggle" data-card-toggle=""></a>
                    </div>

                    <h2 class="card-title">Título</h2>
                    <p class="card-subtitle">
                       Textos e links do banner.
                    </p>
                </header>
                <div class="card-body" style="display: block;">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="form-group row" style="border:none;">
                                <div class="col-lg-8">
                                    <label for="text_1">Texto principal (OBS.: Deixe em branco para não exibir)</label>
                                    <div class="input-group">
                                        <span class="input-group-prepend">
                                            <span class="input-group-text">
                                                <i class="fas fa-align-left"></i>
                                            </span>
                                        </span>
                                        <input id="text_1" value="{{ isset($entity->text_1)!=""?$entity->text_1:'' }}"
                                            type="text" name="text_1" class="form-control" placeholder="Texto">
                                    </div>
                                </div>
                                <div class="col-lg-2">
                                    <label for="font_color_1">Cor</label>
                                    <div class="input-group">
                                        <input id="font_color_1"
                                            value="{{ isset($entity->font_color_1)!=""?$entity->font_color_1:'' }}"
                                            type="text" name="font_color_1" class="form-control" placeholder="Cor">
                                    </div>
                                </div>
                                <div class="col-lg-2">
                                    <label for="font_weight_1">Expessura</label>
                                    <div class="input-group">
                                        <select id="font_weight_1" name="font_weight_1" class="form-control">
                                            <option value="100" {{ (isset($entity->font_weight_1) and $entity->font_weight_1 == "100")?"selected":"" }}>Fino</option>
                                            <option value="300" {{ (isset($entity->font_weight_1) and $entity->font_weight_1 == "300")?"selected":"" }}>Normal</option>
                                            <option value="500" {{ (isset($entity->font_weight_1) and $entity->font_weight_1 == "500")?"selected":"" }}>Negrito</option>
                                            <option value="900" {{ (isset($entity->font_weight_1) and $entity->font_weight_1 == "900")?"selected":"" }}>Extra</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-lg-8">
                                    <label for="text_2">Texto secundário (OBS.: Deixe em branco para não exibir)</label>
                                    <div class="input-group">
                                        <span class="input-group-prepend">
                                            <span class="input-group-text">
                                                <i class="fas fa-align-left"></i>
                                            </span>
                                        </span>
                                        <input id="text_2" value="{{ isset($entity->text_2)!=""?$entity->text_2:'' }}"
                                            type="text" name="text_2" class="form-control" placeholder="Texto">
                                    </div>
                                </div>
                                <div class="col-lg-2">
                                    <label for="font_color_2">Cor</label>
                                    <div class="input-group">
                                        <input id="font_color_2"
                                            value="{{ isset($entity->font_color_2)!=""?$entity->font_color_2:'' }}"
                                            type="text" name="font_color_2" class="form-control" placeholder="Cor">
                                    </div>
                                </div>
                                <div class="col-lg-2">
                                    <label for="font_weight_2">Expessura</label>
                                    <div class="input-group">
                                        <select id="font_weight_2" name="font_weight_2" class="form-control">
                                            <option value="100" {{ (isset($entity->font_weight_2) and $entity->font_weight_2 == "100")?"selected":"" }}>Fino</option>
                                            <option value="300" {{ (isset($entity->font_weight_2) and $entity->font_weight_2 == "300")?"selected":"" }}>Normal</option>
                                            <option value="500" {{ (isset($entity->font_weight_2) and $entity->font_weight_2 == "500")?"selected":"" }}>Negrito</option>
                                            <option value="900" {{ (isset($entity->font_weight_2) and $entity->font_weight_2 == "900")?"selected":"" }}>Extra</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-lg-4">
                                    <label for="button_link">Link (OBS.: deixe em branco para não exibir o botão)</label>
                                    <div class="input-group">
                                        <span class="input-group-prepend">
                                            <span class="input-group-text">
                                                <i class="fa fa-link" aria-hidden="true"></i>
                                            </span>
                                        </span>
                                        <input id="button_link" value="{{ isset($entity->button_link)!=""?$entity->button_link:'' }}"
                                            type="text" name="button_link" class="form-control" placeholder="Texto" aria-describedby="button_link_help">
                                    </div>
                                </div>
                                <div class="col-lg-2">
                                    <label for="button_text">Texto botão</label>
                                    <div class="input-group">
                                        <span class="input-group-prepend">
                                            <span class="input-group-text">
                                                <i class="fas fa-align-left"></i>
                                            </span>
                                        </span>
                                        <input id="button_text" value="{{ isset($entity->button_text)!=""?$entity->button_text:'' }}"
                                            type="text" name="button_text" class="form-control" placeholder="Texto">
                                    </div>
                                </div>
                                <div class="col-lg-2">
                                    <label for="button_text_color">Cor do texto</label>
                                    <div class="input-group">
                                        <input id="button_text_color"
                                            value="{{ isset($entity->button_text_color)!=""?$entity->button_text_color:'' }}"
                                            type="text" name="button_text_color" class="form-control" placeholder="Cor do texto">
                                    </div>
                                </div>
                                <div class="col-lg-2">
                                    <label for="button_bg_color">Cor de fundo</label>
                                    <div class="input-group">
                                        <input id="button_bg_color"
                                            value="{{ isset($entity->button_bg_color)!=""?$entity->button_bg_color:'' }}"
                                            type="text" name="button_bg_color" class="form-control" placeholder="Cor de fundo">
                                    </div>
                                </div>
                                <div class="col-lg-2">
                                    <label for="text_align">Posição do texto</label>
                                    <div class="input-group">
                                        <select id="text_align" name="text_align" class="form-control">
                                            <option value="ml" selected>Esquerda</option>
                                            <option value="mc">Centro</option>
                                            <option value="mr">Direita</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>

            <section class="card">
                <header class="card-header">
                    <div class="card-actions">
                        <a href="forms-basic.html#" class="card-action card-action-toggle" data-card-toggle=""></a>
                    </div>
                    <h2 class="card-title">Banner</h2>
                    <p class="card-subtitle">
                        Selecione a image para a inserção de banner.<br />
                        <br />
                        <strong>Obs.: A imagem selecionada será automatimacamente reajustada para o tamanho na descrição
                            do campo.</strong>
                    </p>
                </header>
                <div class="card-body" style="display: block;">
                    <div class="row">
                        <div class="col-lg-12">
                            <small>Tamanho da imagem 1920px x 800px</small>
                            <div class="fileupload fileupload-new" data-provides="fileupload">
                                <div class="input-append">
                                    <div class="uneditable-input">
                                        <i class="fas fa-file fileupload-exists"></i>
                                        <span class="fileupload-preview"></span>
                                    </div>
                                    <span class="btn btn-default btn-file">
                                        <span class="fileupload-exists">Trocar</span>
                                        <span class="fileupload-new">Selecionar Imagem</span>
                                        <input type="file" name='banner' accept="image/*"
                                            onchange='loadPreview(this, 1920,800)' {{ isset($entity)?'':'required' }}>
                                    </span>
                                    <a href="forms-basic.html#" class="btn btn-default fileupload-exists"
                                        data-dismiss="fileupload">Remove</a>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-12">
                            <textarea id="base64" name="base64" style="display:none;"></textarea>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="img-content">
                                <img id='output' class='img-fluid'
                                    src='{{ isset($entity->image)!=""?"/img/banners/".$entity->image:'' }}'>
                            </div>
                        </div>
                    </div>
                </div>
            </section>

            <section class="card">
                <div class="card-body" style="display: block;">
                    <button type="submit" class="mb-1 mt-1 mr-1 btn btn-success"><i class="fas fa-save"></i>
                        Salvar</button>
                </div>
            </section>
        </form>
    </div>
</div>
@endsection
