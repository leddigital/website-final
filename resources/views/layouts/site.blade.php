<!DOCTYPE html>
<!--[if IE 7]><html class="ie ie7 ltie8 ltie9" lang="en-US"><![endif]-->
<!--[if IE 8]><html class="ie ie8 ltie9" lang="en-US"><![endif]-->
<!--[if !(IE 7) | !(IE 8)  ]><!-->
<html lang="pt-BR">
<!--<![endif]-->

<head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="initial-scale=1.0" />
    <link rel="icon" type="image/png" href="{{ asset('favicon.png') }}" />

    <title>@yield('title')</title>

    <link rel='stylesheet' href='https://fonts.googleapis.com/css?family=Lato%3A100%2C100italic%2C300%2C300italic%2Cregular%2Citalic%2C700%2C700italic%2C900%2C900italic&amp;subset=latin&amp;' type='text/css' media='all' />
    <link rel='stylesheet' href='https://fonts.googleapis.com/css?family=Noto+Sans%3Aregular%2Citalic%2C700%2C700italic&amp;subset=greek%2Ccyrillic-ext%2Ccyrillic%2Clatin%2Clatin-ext%2Cvietnamese%2Cgreek-ext&amp;' type='text/css' media='all' />
    <link rel='stylesheet' href='https://fonts.googleapis.com/css?family=Merriweather%3A300%2C300italic%2Cregular%2Citalic%2C700%2C700italic%2C900%2C900italic&amp;subset=latin%2Clatin-ext&amp;' type='text/css' media='all' />
    <link rel='stylesheet' href='https://fonts.googleapis.com/css?family=Mystery+Quest%3Aregular&amp;subset=latin%2Clatin-ext&amp;' type='text/css' media='all' />

    <link rel='stylesheet' href='{{ asset('css/style.css') }}' type='text/css' media='all' />
    <link rel='stylesheet' href='{{ asset('plugins/superfish/css/superfish.css') }}' type='text/css' media='all' />
    <link rel='stylesheet' href='{{ asset('plugins/dl-menu/component.css') }}' type='text/css' media='all' />
    <link rel='stylesheet' href='{{ asset('plugins/font-awesome-new/css/font-awesome.min.css') }}' type='text/css' media='all' />
    <link rel='stylesheet' href='{{ asset('plugins/elegant-font/style.css') }}' type='text/css' media='all' />
    <link rel='stylesheet' href='{{ asset('plugins/fancybox/jquery.fancybox.css') }}' type='text/css' media='all' />
    <link rel='stylesheet' href='{{ asset('plugins/flexslider/flexslider.css') }}' type='text/css' media='all' />
    <link rel='stylesheet' href='{{ asset('css/style-responsive.css') }}' type='text/css' media='all' />
    <link rel='stylesheet' href='{{ asset('css/style-custom.css') }}' type='text/css' media='all' />
    <link rel='stylesheet' href='{{ asset('plugins/masterslider/public/assets/css/masterslider.main.css') }}' type='text/css' media='all' />
    <link rel='stylesheet' href='{{ asset('css/master-custom.css') }}' type='text/css' media='all' />
    
    <link rel='stylesheet' href='{{ asset('plugins/nivo-slider/themes/default/default.css') }}' type='text/css' media='all' />
    <link rel='stylesheet' href='{{ asset('plugins/nivo-slider/nivo-slider.css') }}' type='text/css' media='all' />
    
    <link rel='stylesheet' href='{{ asset('css/custom.css') }}' type='text/css' media='all' />

    <link rel='stylesheet' href='{{ asset('plugins/Easy-Responsive-Modal-Dialog/simple-modal-default.min.css') }}' type='text/css' media='all' />
    <link rel='stylesheet' href='{{ asset('plugins/Easy-Responsive-Modal-Dialog/simple-modal.min.css') }}' type='text/css' media='all' />

    <link rel='stylesheet' href='{{ asset('plugins/slick-1.8.1/slick/slick.css') }}' type='text/css' media='all' />
    <link rel='stylesheet' href='{{ asset('plugins/slick-1.8.1/slick/slick-theme.css') }}' type='text/css' media='all' />
    
</head>

<body data-rsssl=1 class="home page-template-default page page-id-5680 _masterslider _msp_version_3.2.7 woocommerce-no-js">

    @include('blocks.header')
    @yield('content')
    @include('blocks.footer')

    <script type='text/javascript' src='{{ asset('js/jquery/jquery.js') }}'></script>
    <script type='text/javascript' src='{{ asset('js/jquery/jquery-3.4.1.min.js') }}'></script>


    <script type='text/javascript' src='{{ asset('js/jquery/jquery-migrate.min.js') }}'></script>
    <script>
        var ms_grabbing_curosr = 'plugins/masterslider/public/assets/css/common/grabbing.html',
            ms_grab_curosr = 'plugins/masterslider/public/assets/css/common/grab.html';
    </script>
    <script type='text/javascript' src='{{ asset('plugins/superfish/js/superfish.js') }}'></script>
    <script type='text/javascript' src='{{ asset('js/hoverIntent.min.js') }}'></script>
    <script type='text/javascript' src='{{ asset('plugins/dl-menu/modernizr.custom.js') }}'></script>
    <script type='text/javascript' src='{{ asset('plugins/dl-menu/jquery.dlmenu.js') }}'></script>
    <script type='text/javascript' src='{{ asset('plugins/jquery.easing.js') }}'></script>
    <script type='text/javascript' src='{{ asset('plugins/fancybox/jquery.fancybox.pack.js') }}'></script>
    <script type='text/javascript' src='{{ asset('plugins/fancybox/helpers/jquery.fancybox-media.js') }}'></script>
    <script type='text/javascript' src='{{ asset('plugins/fancybox/helpers/jquery.fancybox-thumbs.js') }}'></script>
    <script type='text/javascript' src='{{ asset('plugins/flexslider/jquery.flexslider.js') }}'></script>
    <script type='text/javascript' src='{{ asset('plugins/jquery.isotope.min.js') }}'></script>
    <script type='text/javascript' src='{{ asset('js/plugins.js') }}'></script>
    <script type='text/javascript' src='{{ asset('plugins/masterslider/public/assets/js/masterslider.min.js') }}'></script>
    <script type='text/javascript' src='{{ asset('plugins/jquery.transit.min.js') }}'></script>
    <script type='text/javascript' src='{{ asset('plugins/gdlr-portfolio/gdlr-portfolio-script.js') }}'></script>
    <script type='text/javascript' src='{{ asset('plugins/jQuery-Mask-Plugin-master/jquery.mask.js') }}'></script>

    <script type='text/javascript' src='{{ asset('plugins/garand-sticky-73b0fbe/jquery.sticky.js') }}'></script>
    

    <script type='text/javascript' src='{{ asset('plugins/slick-1.8.1/slick/slick.min.js') }}'></script>

    <script type='text/javascript' src='{{ asset('plugins/Easy-Responsive-Modal-Dialog/simple-modal.min.js') }}'></script>
    <script type='text/javascript' src='{{ asset('plugins/nivo-slider/jquery.nivo.slider.js') }}'></script>

    <script type='text/javascript' src='{{ asset('js/custom.js') }}'></script>


</body>
</html>
