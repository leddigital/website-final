<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
 */

Route::get('/', 'NavigationController@index')->name('nav.index');
Route::get('/bionat', 'NavigationController@bionat')->name('nav.bionat');
Route::get('/bioforcas', 'NavigationController@bioforcas')->name('nav.bioforcas');
Route::get('/bioforcas/{url}', 'NavigationController@bioforca')->name('nav.bioforca');
Route::get('/mip', 'NavigationController@mip')->name('nav.mip');
Route::get('/bionews', 'NavigationController@bionews')->name('nav.bionews');
Route::get('/bionews/procurar', 'NavigationController@procurar')->name('nav.procurar');
Route::get('/bionews/{url}', 'NavigationController@singlepost')->name('nav.singlepost');
Route::get('/contato', 'NavigationController@contato')->name('nav.contato');
Route::get('/equipe-comercial', 'NavigationController@presenca')->name('nav.equipe-comercial');
Route::get('/biblionat', 'NavigationController@biblionat')->name('nav.biblionat');
Route::get('/calculonat', 'NavigationController@calculonat')->name('nav.calculonat');

//Enviar e-mail
Route::post('/enviar/email', 'EmailController@email')->name('send.mail');

//Cadastrar Newsletter
Route::post('/cadastrar/newsletter', 'NewsletterController@cadastrar')->name('sign.newsletter');

Auth::routes();

//Rotas para utilizacao de auten
Route::group(['prefix' => 'admin', 'middleware' => ['auth']], function () {

    //Informações Gerais
    Route::get('/', 'InformationsController@index')->name('information.index');
    Route::post('/informacoes/salvar', 'InformationsController@save')->name('information.save');

    //Categorias
    Route::get('/categorias', 'CategoriesController@index')->name('categories.index');
    Route::get('/categorias/listar/todos', 'CategoriesController@readAll')->name('categories.list.all');
    Route::get('/categorias/editar/{id?}', 'CategoriesController@form')->name('categories.edit');
    Route::get('/categorias/cadastro', 'CategoriesController@form')->name('categories.form');
    Route::post('/categorias/salvar', 'CategoriesController@save')->name('categories.save');
    Route::post('/categorias/editar/salvar/{id}', 'CategoriesController@save')->name('categories.edit.save');
    Route::get('/categorias/deletar/{id?}', 'CategoriesController@delete')->name('categories.delete');

    //Banners
    Route::get('/banners', 'BannersController@index')->name('banners.index');
    Route::get('/banners/listar/todos', 'BannersController@readAll')->name('banners.list.all');
    Route::get('/banners/editar/{id?}', 'BannersController@form')->name('banners.edit');
    Route::get('/banners/cadastro', 'BannersController@form')->name('banners.form');
    Route::post('/banners/salvar', 'BannersController@save')->name('banners.save');
    Route::post('/banners/editar/salvar/{id}', 'BannersController@save')->name('banners.edit.save');
    Route::get('/banners/deletar/{id?}', 'BannersController@delete')->name('banners.delete');

    //Conteudo
    Route::get('/conteudo/{_type}', 'ContentsController@index')->name('content.index');
    Route::get('/conteudo/listar/todos/{_type}', 'ContentsController@readAll')->name('content.list.all');
    Route::get('/conteudo/editar/{_type}/{id?}', 'ContentsController@form')->name('content.edit');
    Route::get('/conteudo/cadastro/{_type}', 'ContentsController@form')->name('content.form');
    Route::post('/conteudo/salvar', 'ContentsController@save')->name('content.save');
    Route::post('/conteudo/editar/salvar/{id}', 'ContentsController@save')->name('content.edit.save');
    Route::get('/conteudo/deletar/{id?}', 'ContentsController@delete')->name('content.delete');

    //Contatos
    Route::get('/contato/index', 'ContactsController@index')->name('contato.index');
    Route::get('/contato/index/cadastro', 'ContactsController@form')->name('contato.form');
    Route::post('/contato/salvar', 'ContactsController@save')->name('contato.save');
    Route::post('/contato/editar/salvar/{id}', 'ContactsController@save')->name('contato.edit.save');
    Route::get('/contato/editar/{id?}', 'ContactsController@form')->name('contato.edit');
    Route::get('/contato/delete/{id?}', 'ContactsController@delete')->name('contato.delete');
    Route::get('/contato/listar/todos', 'ContactsController@readAll')->name('contato.list.all');

    //Posts
    Route::get('/posts/index', 'PostsController@index')->name('posts.index');
    Route::get('/posts/index/cadastro', 'PostsController@form')->name('posts.form');
    Route::post('/posts/salvar', 'PostsController@save')->name('posts.save');
    Route::post('/posts/editar/salvar/{id}', 'PostsController@save')->name('posts.edit.save');
    Route::get('/posts/editar/{id?}', 'PostsController@form')->name('posts.edit');
    Route::get('/posts/delete/{id?}', 'PostsController@delete')->name('posts.delete');
    Route::get('/posts/listar/todos', 'PostsController@readAll')->name('posts.list.all');

});
