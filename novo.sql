-- MySQL dump 10.13  Distrib 5.7.28, for Linux (x86_64)
--
-- Host: localhost    Database: bionat
-- ------------------------------------------------------
-- Server version	5.7.28-0ubuntu0.18.04.4

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `banners`
--

DROP TABLE IF EXISTS `banners`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `banners` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `image` text NOT NULL,
  `title` varchar(255) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `status` tinyint(4) DEFAULT NULL,
  `text_1` varchar(255) DEFAULT NULL,
  `font_color_1` varchar(255) DEFAULT NULL,
  `font_weight_1` varchar(255) DEFAULT NULL,
  `text_2` varchar(255) DEFAULT NULL,
  `font_color_2` varchar(255) DEFAULT NULL,
  `font_weight_2` varchar(255) DEFAULT NULL,
  `button_link` varchar(255) DEFAULT NULL,
  `button_text` varchar(255) DEFAULT NULL,
  `button_bg_color` varchar(255) DEFAULT NULL,
  `text_align` varchar(255) DEFAULT NULL,
  `button_text_color` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `banners`
--

LOCK TABLES `banners` WRITE;
/*!40000 ALTER TABLE `banners` DISABLE KEYS */;
INSERT INTO `banners` VALUES (19,'banner_5e1855b6c20fa.jpeg','Banner 1','2020-01-10 10:45:10','2020-01-10 10:45:10',NULL,NULL,NULL,'100',NULL,NULL,'100',NULL,NULL,NULL,'ml',NULL),(20,'banner_5e1855c1ee68b.jpeg','Banner 2','2020-01-10 10:45:21','2020-01-10 10:45:21',NULL,NULL,NULL,'100',NULL,NULL,'100',NULL,NULL,NULL,'ml',NULL),(21,'banner_5e1855ccb08eb.jpeg','Banner 3','2020-01-10 10:45:32','2020-01-10 10:45:32',NULL,NULL,NULL,'100',NULL,NULL,'100',NULL,NULL,NULL,'ml',NULL);
/*!40000 ALTER TABLE `banners` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `categories`
--

DROP TABLE IF EXISTS `categories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `categories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) DEFAULT NULL,
  `url` varchar(255) DEFAULT NULL,
  `type` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `categories`
--

LOCK TABLES `categories` WRITE;
/*!40000 ALTER TABLE `categories` DISABLE KEYS */;
INSERT INTO `categories` VALUES (1,'Qualquer coisa','qualquer-coisa','news'),(2,'Bio Ciências','bio-ciencias','news');
/*!40000 ALTER TABLE `categories` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `categories_has_contents`
--

DROP TABLE IF EXISTS `categories_has_contents`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `categories_has_contents` (
  `categories_id` int(11) NOT NULL,
  `contents_id` int(11) NOT NULL,
  PRIMARY KEY (`categories_id`,`contents_id`),
  KEY `fk_categories_has_contents_contents1_idx` (`contents_id`),
  KEY `fk_categories_has_contents_categories1_idx` (`categories_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `categories_has_contents`
--

LOCK TABLES `categories_has_contents` WRITE;
/*!40000 ALTER TABLE `categories_has_contents` DISABLE KEYS */;
INSERT INTO `categories_has_contents` VALUES (1,1),(2,1),(1,2);
/*!40000 ALTER TABLE `categories_has_contents` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `contacts`
--

DROP TABLE IF EXISTS `contacts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `contacts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL DEFAULT 'null',
  `role` varchar(255) NOT NULL DEFAULT 'null',
  `email` varchar(255) NOT NULL DEFAULT 'null',
  `contact` varchar(255) NOT NULL DEFAULT 'null',
  `address` varchar(255) NOT NULL DEFAULT 'null',
  `state` varchar(45) NOT NULL DEFAULT 'null',
  `created_at` varchar(255) DEFAULT NULL,
  `updated_at` varchar(255) DEFAULT NULL,
  `estado` varchar(255) NOT NULL DEFAULT 'null',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `contacts`
--

LOCK TABLES `contacts` WRITE;
/*!40000 ALTER TABLE `contacts` DISABLE KEYS */;
INSERT INTO `contacts` VALUES (7,'Guedes Ferreira do Prado','Coordenador Regional de Vendas','guedes.prado@bionatagro.com','(66) 99951-8267','Sorriso/MT','MT','2019-12-17 13:08:54','2020-01-09 14:45:43','Mato Grosso'),(8,'Diego Ramos Bicudo','Coordenador Regional de Vendas','diego.bicudo@bionatagro.com','34 99912-8303','Uberlândia/MG','MG','2019-12-17 13:09:12','2020-01-09 14:46:00','Minas Gerais'),(9,'Sandro Moreira Marcantes','Consultor Técnico Comercial','sandro.marcantes@bionatagro.com.br','43 99146-4867','Ponta Grossa/PR','PR','2019-12-17 13:09:30','2020-01-09 14:44:59','Paraná'),(10,'Helena Mendes de Freitas','Consultora Técnica Comercial','helena.freitas@bionatagro.com','66 99725-0558','Sinop/MT','MT','2019-12-17 13:09:49','2020-01-09 14:45:37','Mato Grosso'),(11,'Galvão Aleixo','Consultor Técnico Comercial','galvao.aleixo@bionatagro.com','66 99641-3036','Campo Novo do Parecis/MT','MT','2019-12-17 13:10:05','2020-01-09 14:46:09','Mato Grosso'),(12,'Sandro Moreira Marcantes','Consultor Técnico Comercial','sandro.marcantes@bionatagro.com.br','43 99146-4867','Olímpia/SP','SP','2019-12-17 13:10:32','2020-01-09 14:45:31','São Paulo');
/*!40000 ALTER TABLE `contacts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `contents`
--

DROP TABLE IF EXISTS `contents`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `contents` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `short_description` varchar(255) DEFAULT NULL,
  `content` longtext,
  `type` varchar(45) NOT NULL COMMENT 'post,news,page,page-name',
  `url` varchar(45) NOT NULL,
  `image` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `categoria` varchar(255) DEFAULT NULL,
  `embalagem` varchar(255) DEFAULT NULL,
  `composicao` varchar(255) DEFAULT NULL,
  `bula` varchar(255) DEFAULT NULL,
  `concentracao` varchar(255) DEFAULT NULL,
  `desc_recomendacao` text,
  `desc_composicao` text,
  `desc_funciona` text,
  `desc_aplicar` text,
  `desc_compatibilidade` text,
  `desc_armazenamento` text,
  `desc_transporte` text,
  `desc_cultura` text,
  `desc_quando` text,
  PRIMARY KEY (`id`),
  UNIQUE KEY `url_UNIQUE` (`url`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `contents`
--

LOCK TABLES `contents` WRITE;
/*!40000 ALTER TABLE `contents` DISABLE KEYS */;
INSERT INTO `contents` VALUES (1,'Bovenat','É o inseticida microbiano à base de isolado Beauveria bassiana, microrganismo generalista que apresenta alta eficiência, capacidade de penetração e colonização de insetos e ácaros pragas.','<p><span style=\"font-family: Arial;\"><b>﻿</b></span><font face=\"Arial\"><b>Produto:&nbsp;&nbsp;</b><br></font><span style=\"font-family: Arial;\">Bovenat&nbsp;</span></p><p><font face=\"Arial\"><b>Descritivo:&nbsp;</b>&nbsp;<br></font><span style=\"font-family: Arial;\">É o inseticida microbiano à base de isolado Beauveria bassiana, microrganismo generalista que apresenta alta eficiência, capacidade de penetração e colonização de insetos e ácaros pragas.&nbsp;</span></p><p><font face=\"Arial\"><b>Categoria:&nbsp;&nbsp;</b><br></font><span style=\"font-family: Arial;\">Inseticida e acaricida microbiano.</span></p><p><span style=\"font-family: Arial;\"><b>Embalagem:</b>&nbsp;<br></span><span style=\"font-family: Arial;\">Saco aluminizado sanfonado com capacidade de 1 Kg.</span></p><p><font face=\"Arial\"><b>Concentração:&nbsp;</b><br></font><span style=\"font-family: Arial;\">1 x 1010 UFC/g.</span></p><p><font face=\"Arial\"><b>Recomendação:&nbsp;&nbsp;</b><br></font><span style=\"font-family: Arial;\">Indicado para aplicação foliar para o controle da mosca branca (Bemisia tabaci raça B), cigarrinha do milho (Dalbulus maidis), ácaro rajado (Tetranychus urticae) e aplicação via iscas para Cosmopolites sordidus (Vide bula).</span></p><p><font face=\"Arial\"><b>Composição:&nbsp;&nbsp;</b><br></font><span style=\"font-family: Arial;\">Beauveria bassiana cepa IBCB 66 - 1,0 x 1010 UFC/g 110g/Kg (11,0%)<br></span><span style=\"font-family: Arial;\">Outros ingredientes - 890g/Kg (89,0%)</span></p><p><font face=\"Arial\"><b>Cultura Recomendada:</b><br></font><span style=\"font-family: Arial;\">O produto apresenta eficiência agronômica comprovada nas culturas da soja, pepino, banana, morango, milho e pode ser utilizado em qualquer outra cultura com ocorrência dos alvos biológicos.</span></p><p><span style=\"font-family: Arial;\"><b>Quando utilizar:&nbsp;&nbsp;<br></b></span><span style=\"font-family: Arial;\">Nas culturas com a ocorrência do alvo biológico, quando o a população da praga atingir o nível de controle.</span></p><p><font face=\"Arial\"><b>Como funciona (Mecanismo de ação):&nbsp;&nbsp;</b><br></font><span style=\"font-family: Arial;\">O conídio do fungo B. bassiana é o ingrediente ativo do produto (tamanho dos conídios de 1 a 3 micrometros). Quando pulverizados, entram em contato com o corpo do inseto, os conídios aderidos tendo condições favoráveis (umidade e temperatura), germinam, formando uma estrutura chamada de “tubo germinativo”, que por conseguinte forma uma outra estrutura, estrutura de fixação e penetração (apressório). O apressório fixa o fungo na cutícula do inseto, produz enzimas quitinases e lipases que degradam a cutícula, ao mesmo momento o apressório produz uma pressão mecânica que funciona para romper a cutícula do inseto. Rompendo a cutícula do inseto, o fungo passa a produzir toxinas e estruturas fúngicas de colonização (blastosporos, corpos hifais, etc.) que são liberadas na hemocele do inseto (corrente sanguínea do inseto). As toxinas debilitam o sistema imune do inseto, que para de alimentar, levando-o a morte. No mesmo momento, os blastósporos circula pela hemocele, colonizando o inseto internamente. O fungo consume todos os nutrientes e minerais do corpo do inseto, nesse momento o inseto já está morto (período que pode vária de 3 a 7 dias). Acabando os nutrientes do corpo do inseto, o fungo precisa se reproduzir e disseminar. Dessa forma, o fungo rompe o tecido do inseto no sentido oposto (saída do patógeno), extrusão do entomopatógeno. Em seguida, com umidade alta e temperatura entre 22 a 30°C o fungo forma novos conídios, semelhantes com aqueles aplicados via pulverização. Esses conídios são disseminados pelo vento e chuva no ambiente e podem provocar a morte de outros insetos aumento ainda mais a eficiência de controle no campo.</span></p><p><span style=\"font-family: Arial;\"><b>Como aplicar:&nbsp;&nbsp;</b></span><br></p><p><font face=\"Arial\">- Antes de realizar o preparo da calda de pulverização certificar da limpeza do pulverizador. Caso o pulverizador apresente resíduos de produtos de aplicações anteriores (principalmente fungicida, bactericidas e inseticidas incompatíveis) é de fundamental importância a limpeza do equipamento, pois pode afetar o desempenho do microrganismo;</font></p><p><font face=\"Arial\">-Colocar a formulação do adjuvante potencializador no balde (ADbio fungi);</font></p><p><font face=\"Arial\">-Colocar aos poucos o produto Bovenat no balde e ir misturando;</font></p><p><font face=\"Arial\">-Adicionar água, agitar e em seguida despejar no pulverizador contendo água.</font></p><p><font face=\"Arial\">-Recomenda-se que se inicie a aplicação logo após o preparo da calda de pulverização.</font></p><p><font face=\"Arial\">-Para a aplicação pode-se utilizar pulverizador costal, tratorizado ou aéreo.</font></p><p><font face=\"Arial\">-Efetuar as aplicações de forma que possibilitem uma boa cobertura da parte aérea das plantas com a presença da praga alvo, sem causar escorrimento. Sendo o volume de calda variável de acordo com a espécie de planta.</font></p><p><font face=\"Arial\">-Recomenda-se aplicar nas horas mais frescas do dia, preferencialmente no final da tarde. Evitar aplicação em condição de temperatura acima de 27°C ou na presença de ventos fortes (velocidade acima de 10 Km/hora), bem como com umidade relativa do ar abaixo de 70% ou com alta intensidade de radiação ultravioleta (UV).</font></p><p><font face=\"Arial\">-A escolha dos equipamentos a serem utilizados para aplicação deste produto poderá sofrer alterações a critério do Engenheiro Agrônomo, tomando-se o cuidado de evitar sempre à deriva e perdas do produto por evaporação.</font></p><p><span style=\"font-family: Arial;\">- No caso do moleque da bananeira (Cosmopolites sordidus)- Preparo das Iscas: Diluir 500g do produto em 5 litros de água ou óleo vegetal formando uma pasta homogênea. Aplicar a pasta sobre toda a superfície seccionada de iscas-atrativas (tipo telha ou queijo). Distribuir na base das plantas 100 iscas/ha, mantendo-as com a superfície tratada voltada para o solo. Substituir as iscas em intervalos de 15 dias, observando o limite máximo de 3 aplicações.</span></p><p><font face=\"Arial\">Compatibilidade:&nbsp;<br></font><span style=\"font-family: Arial;\">Antes de realizar qualquer mistura de produto na calda de aplicação com o Bovenat, é de fundamental importância ver a compatibilidade desses produtos com o fungo Beauveria bassiana. Produtos classificado como moderamente tóxicos e tóxicos ao fungo entomopatogênico, prejudicam a performance do microrganismo, matando-o. Em breve, teremos a lista de compatibilidade com a maioria dos defensivos químicos e fertilizantes foliares utilizados na agricultura.</span></p><p><font face=\"Arial\"><b>Armazenamento:</b></font></p><p><font face=\"Arial\">- Mantenha o produto em sua embalagem origi­nal, sempre fechada.</font></p><p><font face=\"Arial\">- O local deve ser exclusivo para produtos tóxi­cos, devendo ser isolado de produtos químicos, alimentos, bebidas, rações ou outros materiais.</font></p><p><font face=\"Arial\">- A construção deve ser de alvenaria ou de mate­rial não combustível.</font></p><p><font face=\"Arial\">- O local deve ser ventilado, coberto e ter piso impermeável.</font></p><p><font face=\"Arial\">- Tranque o local, evitando o acesso de pessoas não autorizadas, principalmente crianças.</font></p><p><font face=\"Arial\">- Deve haver sempre embalagens adequadas dis­poníveis para envolver embalagens rompidas ou para o recolhimento de produtos vazados.</font></p><p><font face=\"Arial\">- Em caso de armazéns, deverão ser seguidas as instruções constantes da NBR 9843, da Associa­ção Brasileira de Normas Técnicas - ABNT.</font></p><p><font face=\"Arial\">- Observe as disposições constantes da legisla­ção estadual e municipal.</font></p><p><font face=\"Arial\">- Armazenar sob refrigeração a 5°C ou -15°C por até 120 dias, e a 27°C por até 90 dias.</font></p><p><font face=\"Arial\"><b>Transporte:</b><br></font><span style=\"font-family: Arial;\">As embalagens vazias não podem ser transporta­das junto com alimentos, bebidas, medicamentos, rações, animais e pessoas. Devem ser transporta­das em saco plástico transparente (Embalagens Padronizadas – modelo ABNT), devidamente identificado e com lacre, o qual deverá ser adqui­rido nos Canais de Distribuição.</span></p>','bioforca','bovenat','bovenat.png','2019-12-18 15:54:09','2019-12-18 15:54:09','Inseticida e acaricida microbiano','Saco aluminizado sanfonado com capacidade de 1 Kg','Beauveria bassiana','Saco aluminizado sanfonado com capacidade de 1 Kg','1 x 1010 UFC/g','Indicado para aplicação foliar para o controle da mosca branca (Bemisia tabaci raça B), cigarrinha do milho (Dalbulus maidis), ácaro rajado (Tetranychus urticae) e aplicação via iscas para Cosmopolites sordidus (Vide bula).<br>','<p>Beauveria bassiana cepa IBCB 66 - 1,0 x 1010 UFC/g 110g/Kg (11,0%)<br>Outros ingredientes - 890g/Kg (89,0%)</p>','<p>O conídio do fungo B. bassiana é o ingrediente ativo do produto (tamanho dos conídios de 1 a 3 micrometros). Quando pulverizados, entram em contato com o corpo do inseto, os conídios aderidos tendo condições favoráveis (umidade e temperatura), germinam, formando uma estrutura chamada de “tubo germinativo”, que por conseguinte forma uma outra estrutura, estrutura de fixação e penetração (apressório). O apressório fixa o fungo na cutícula do inseto, produz enzimas quitinases e lipases que degradam a cutícula, ao mesmo momento o apressório produz uma pressão mecânica que funciona para romper a cutícula do inseto. Rompendo a cutícula do inseto, o fungo passa a produzir toxinas e estruturas fúngicas de colonização (blastosporos, corpos hifais, etc.) que são liberadas na hemocele do inseto (corrente sanguínea do inseto). As toxinas debilitam o sistema imune do inseto, que para de alimentar, levando-o a morte. No mesmo momento, os blastósporos circula pela hemocele, colonizando o inseto internamente. O fungo consume todos os nutrientes e minerais do corpo do inseto, nesse momento o inseto já está morto (período que pode vária de 3 a 7 dias). Acabando os nutrientes do corpo do inseto, o fungo precisa se reproduzir e disseminar. Dessa forma, o fungo rompe o tecido do inseto no sentido oposto (saída do patógeno), extrusão do entomopatógeno. Em seguida, com umidade alta e temperatura entre 22 a 30°C o fungo forma novos conídios, semelhantes com aqueles aplicados via pulverização. Esses conídios são disseminados pelo vento e chuva no ambiente e podem provocar a morte de outros insetos aumento ainda mais a eficiência de controle no campo.<br></p>','<p>- Antes de realizar o preparo da calda de pulverização certificar da limpeza do pulverizador. Caso o pulverizador apresente resíduos de produtos de aplicações anteriores (principalmente fungicida, bactericidas e inseticidas incompatíveis) é de fundamental importância a limpeza do equipamento, pois pode afetar o desempenho do microrganismo;</p><p>-Colocar a formulação do adjuvante potencializador no balde (ADbio fungi);</p><p>-Colocar aos poucos o produto Bovenat no balde e ir misturando;</p><p>-Adicionar água, agitar e em seguida despejar no pulverizador contendo água.</p><p>-Recomenda-se que se inicie a aplicação logo após o preparo da calda de pulverização.</p><p>-Para a aplicação pode-se utilizar pulverizador costal, tratorizado ou aéreo.</p><p>-Efetuar as aplicações de forma que possibilitem uma boa cobertura da parte aérea das plantas com a presença da praga alvo, sem causar escorrimento. Sendo o volume de calda variável de acordo com a espécie de planta.</p><p>-Recomenda-se aplicar nas horas mais frescas do dia, preferencialmente no final da tarde. Evitar aplicação em condição de temperatura acima de 27°C ou na presença de ventos fortes (velocidade acima de 10 Km/hora), bem como com umidade relativa do ar abaixo de 70% ou com alta intensidade de radiação ultravioleta (UV).</p><p>-A escolha dos equipamentos a serem utilizados para aplicação deste produto poderá sofrer alterações a critério do Engenheiro Agrônomo, tomando-se o cuidado de evitar sempre à deriva e perdas do produto por evaporação.</p><p><br></p><p>- No caso do moleque da bananeira (Cosmopolites sordidus)- Preparo das Iscas: Diluir 500g do produto em 5 litros de água ou óleo vegetal formando uma pasta homogênea. Aplicar a pasta sobre toda a superfície seccionada de iscas-atrativas (tipo telha ou queijo). Distribuir na base das plantas 100 iscas/ha, mantendo-as com a superfície tratada voltada para o solo. Substituir as iscas em intervalos de 15 dias, observando o limite máximo de 3 aplicações.</p>','<p>Antes de realizar qualquer mistura de produto na calda de aplicação com o Bovenat, é de fundamental importância ver a compatibilidade desses produtos com o fungo Beauveria bassiana. Produtos classificado como moderamente tóxicos e tóxicos ao fungo entomopatogênico, prejudicam a performance do microrganismo, matando-o. Em breve, teremos a lista de compatibilidade com a maioria dos defensivos químicos e fertilizantes foliares utilizados na agricultura.</p>','<p>- Mantenha o produto em sua embalagem origi­nal, sempre fechada.</p><p>- O local deve ser exclusivo para produtos tóxi­cos, devendo ser isolado de produtos químicos, alimentos, bebidas, rações ou outros materiais.</p><p>- A construção deve ser de alvenaria ou de mate­rial não combustível.</p><p>- O local deve ser ventilado, coberto e ter piso impermeável.</p><p>- Tranque o local, evitando o acesso de pessoas não autorizadas, principalmente crianças.</p><p>- Deve haver sempre embalagens adequadas dis­poníveis para envolver embalagens rompidas ou para o recolhimento de produtos vazados.</p><p>- Em caso de armazéns, deverão ser seguidas as instruções constantes da NBR 9843, da Associa­ção Brasileira de Normas Técnicas - ABNT.</p><p>- Observe as disposições constantes da legisla­ção estadual e municipal.</p><p>- Armazenar sob refrigeração a 5°C ou -15°C por até 120 dias, e a 27°C por até 90 dias.</p>','<p>As embalagens vazias não podem ser transporta­das junto com alimentos, bebidas, medicamentos, rações, animais e pessoas. Devem ser transporta­das em saco plástico transparente (Embalagens Padronizadas – modelo ABNT), devidamente identificado e com lacre, o qual deverá ser adqui­rido nos Canais de Distribuição.<br></p>','<p>O produto apresenta eficiência agronômica comprovada nas culturas da soja, pepino, banana, morango, milho e pode ser utilizado em qualquer outra cultura com ocorrência dos alvos biológicos.<br></p>','<p>Nas culturas com a ocorrência do alvo biológico, quando o a população da praga atingir o nível de controle.<br></p>'),(2,'Metarhizonat','É o inseticida microbiano a base do fungo Metarhizium anisopliae, microrganismo que infecta e mata uma ampla gama de espécies pragas, principalmente os insetos que vivem próximo ao solo.','<p><b><span style=\"font-family: Arial;\">Produto:&nbsp; </span></b><br>Metarhizonat&nbsp;&nbsp;</p><p><b>Descritivo:&nbsp; </b><br>É o inseticida microbiano a base do fungo Metarhizium anisopliae, microrganismo que infecta e mata uma ampla gama de espécies pragas, principalmente os insetos que vivem próximo ao solo.</p><p><b>Categoria:&nbsp; </b><br>Inseticida microbiano</p><p><b>Embalagem: </b><br>0,5 Kg</p><p><b>Concentração: </b><br>4,0 x 1010 conídios viáveis/g</p><p><b>Recomendação:&nbsp; </b><br>Indicado para aplicação foliar para o controle da cigarrinha-da-raiz (Mahanarva fimbriolata), no controle da cigarrinha-das-pastagens (Zulia entreriana) e no controle da cigarrinha-dos-capinzais (Deois flavopicta) (Vide bula).</p><p><b>Composição:&nbsp; </b><br>Metarhizium anisopliae cepa IBCB 425 (mínimo de 4,0 x 1010 conídios viáveis/g) 900g/Kg (90%)<br>Outros ingredientes 100g/Kg (10%)</p><p><b>Cultura Recomendada:</b><br>O produto apresenta eficiência agronômica comprovada nas culturas da cana-de-açúcar, pastagens, pastagem de capim braquiária e pode ser utilizado em qualquer outra cultura com ocorrência dos alvos biológicos.</p><p><b>Quando utilizar:&nbsp; </b><br>Nas culturas com a ocorrência do alvo biológico, quando o a população da praga atingir o nível de controle.</p><p>Como funciona (Mecanismo de ação): <br>O conídio do fungo M. anisopliae é o ingrediente ativo do produto (tamanho dos conídios de 1 a 3 micrometros). Quando pulverizados, entram em contato com o corpo do inseto, os conídios aderidos tendo condições favoráveis (umidade e temperatura), germinam, formando uma estrutura chamada de “tubo germinativo”, que por conseguinte forma uma outra estrutura, estrutura de fixação e penetração (apressório). O apressório fixa o fungo na cutícula do inseto, produz enzimas quitinases e lipases que degradam a cutícula, ao mesmo momento o apressório produz uma pressão mecânica que funciona para romper a cutícula do inseto. Rompendo a cutícula do inseto, o fungo passa a produzir toxinas e estruturas fúngicas de colonização (blastosporos, corpos hifais, etc.) que são liberadas na hemocele do inseto (corrente sanguínea do inseto). As toxinas debilitam o sistema imune do inseto, que para de alimentar, levando-o a morte. No mesmo momento, os blastósporos circula pela hemocele, colonizando o inseto internamente. O fungo consume todos os nutrientes e minerais do corpo do inseto, nesse momento o inseto já está morto (período que pode vária de 3 a 7 dias). Acabando os nutrientes do corpo do inseto, o fungo precisa se reproduzir e disseminar. Dessa forma, o fungo rompe o tecido do inseto no sentido oposto (saída do patógeno), extrusão do entomopatógeno. Em seguida, com umidade alta e temperatura entre 22 a 30°C o fungo forma novos conídios, semelhantes com aqueles aplicados via pulverização. Esses conídios são disseminados pelo vento e chuva no ambiente e podem provocar a morte de outros insetos aumento ainda mais a eficiência de controle no campo.</p><p><br></p><p><b>Como aplicar:&nbsp;&nbsp;</b><br></p><p>- Antes de realizar o preparo da calda de pulverização certificar da limpeza do pulverizador. Caso o pulverizador apresente resíduos de produtos de aplicações anteriores (principalmente fungicida, bactericidas e inseticidas incompatíveis) é de fundamental importância a limpeza do equipamento, pois pode afetar o desempenho do produto;</p><p>-Colocar a formulação do adjuvante potencializador no balde (ADbio fungi);</p><p>-Colocar aos poucos o produto Metarhizonat no balde e ir misturando;</p><p>-Adicionar água, agitar e em seguida despejar no pulverizador contendo água.</p><p>-Recomenda-se que se inicie a aplicação logo após o preparo da calda de pulverização.</p><p>-Para a aplicação pode-se utilizar pulverizador costal, tratorizado ou aéreo.</p><p>-Efetuar as aplicações de forma que possibilitem uma boa cobertura da parte aérea das plantas com a presença da praga alvo, sem causar escorrimento. Sendo o volume de calda variável de acordo com a espécie de planta.</p><p>-Recomenda-se aplicar nas horas mais frescas do dia, preferencialmente no final da tarde. Evitar aplicação em condição de temperatura acima de 27°C ou na presença de ventos fortes (velocidade acima de 10 Km/hora), bem como com umidade relativa do ar abaixo de 70% ou com alta intensidade de radiação ultravioleta (UV).</p><p>-A escolha dos equipamentos a serem utilizados para aplicação deste produto poderá sofrer alterações a critério do Engenheiro Agrônomo, tomando-se o cuidado de evitar sempre à deriva e perdas do produto por evaporação.</p><p><br></p><p><b>COMPATIBILIDADE:</b></p><p>Antes de realizar qualquer mistura de produto na calda de aplicação com o Metarhizonat, é de fundamental importância ver a compatibilidade desses produtos com o fungo Metarhizium anisopliae. Produtos classificado como moderamente tóxicos e tóxicos ao fungo entomopatogênico, prejudicam a performance do microrganismo, matando-o. Em breve, teremos a lista de compatibilidade com a maioria dos defensivos químicos e fertilizantes foliares utilizados na agricultura.</p><p><br></p><p><b>ARMAZENAMENTO:</b><br></p><p>- Mantenha o produto em sua embalagem origi­nal, sempre fechada.</p><p>- O local deve ser exclusivo para produtos tóxi­cos, devendo ser isolado de produtos químicos, alimentos, bebidas, rações ou outros materiais.</p><p>- A construção deve ser de alvenaria ou de mate­rial não combustível.</p><p>- O local deve ser ventilado, coberto e ter piso impermeável.</p><p>- Tranque o local, evitando o acesso de pessoas não autorizadas, principalmente crianças.</p><p>- Deve haver sempre embalagens adequadas dis­poníveis para envolver embalagens rompidas ou para o recolhimento de produtos vazados.</p><p>- Em caso de armazéns, deverão ser seguidas as instruções constantes da NBR 9843, da Associa­ção Brasileira de Normas Técnicas - ABNT.</p><p>- Observe as disposições constantes da legisla­ção estadual e municipal.</p><p>- Armazenar sob refrigeração a 27°C, 5°C ou -15°C por até 150 dias.</p><p><br></p><p><b>Transporte:</b><br>As embalagens vazias não podem ser transporta­das junto com alimentos, bebidas, medicamentos, rações, animais e pessoas. Devem ser transporta­das em saco plástico transparente (Embalagens Padronizadas – modelo ABNT), devidamente identificado e com lacre, o qual deverá ser adqui­rido nos Canais de Distribuição.</p>','bioforca','metarhizonat','metarhizonat.png','2019-12-18 15:54:09','2019-12-18 15:54:09','Inseticida microbiano','0,5 Kg','Metarhizium anisopliae','Saco aluminizado sanfonado com capacidade de 1 Kg',' 4,0 x 1010 conídios viáveis/g','<p>Indicado para aplicação foliar para o controle da cigarrinha-da-raiz (Mahanarva fimbriolata), no controle da cigarrinha-das-pastagens (Zulia entreriana) e no controle da cigarrinha-dos-capinzais (Deois flavopicta) (Vide bula).<br></p>','<p>Metarhizium anisopliae cepa IBCB 425 (mínimo de 4,0 x 1010 conídios viáveis/g) 900g/Kg (90%)<br>Outros ingredientes 100g/Kg (10%)</p>','<p>O conídio do fungo M. anisopliae é o ingrediente ativo do produto (tamanho dos conídios de 1 a 3 micrometros). Quando pulverizados, entram em contato com o corpo do inseto, os conídios aderidos tendo condições favoráveis (umidade e temperatura), germinam, formando uma estrutura chamada de “tubo germinativo”, que por conseguinte forma uma outra estrutura, estrutura de fixação e penetração (apressório). O apressório fixa o fungo na cutícula do inseto, produz enzimas quitinases e lipases que degradam a cutícula, ao mesmo momento o apressório produz uma pressão mecânica que funciona para romper a cutícula do inseto. Rompendo a cutícula do inseto, o fungo passa a produzir toxinas e estruturas fúngicas de colonização (blastosporos, corpos hifais, etc.) que são liberadas na hemocele do inseto (corrente sanguínea do inseto). As toxinas debilitam o sistema imune do inseto, que para de alimentar, levando-o a morte. No mesmo momento, os blastósporos circula pela hemocele, colonizando o inseto internamente. O fungo consume todos os nutrientes e minerais do corpo do inseto, nesse momento o inseto já está morto (período que pode vária de 3 a 7 dias). Acabando os nutrientes do corpo do inseto, o fungo precisa se reproduzir e disseminar. Dessa forma, o fungo rompe o tecido do inseto no sentido oposto (saída do patógeno), extrusão do entomopatógeno. Em seguida, com umidade alta e temperatura entre 22 a 30°C o fungo forma novos conídios, semelhantes com aqueles aplicados via pulverização. Esses conídios são disseminados pelo vento e chuva no ambiente e podem provocar a morte de outros insetos aumento ainda mais a eficiência de controle no campo.<br></p>','<p>- Antes de realizar o preparo da calda de pulverização certificar da limpeza do pulverizador. Caso o pulverizador apresente resíduos de produtos de aplicações anteriores (principalmente fungicida, bactericidas e inseticidas incompatíveis) é de fundamental importância a limpeza do equipamento, pois pode afetar o desempenho do produto;</p><p>-Colocar a formulação do adjuvante potencializador no balde (ADbio fungi);</p><p>-Colocar aos poucos o produto Metarhizonat no balde e ir misturando;</p><p>-Adicionar água, agitar e em seguida despejar no pulverizador contendo água.</p><p>-Recomenda-se que se inicie a aplicação logo após o preparo da calda de pulverização.</p><p>-Para a aplicação pode-se utilizar pulverizador costal, tratorizado ou aéreo.</p><p>-Efetuar as aplicações de forma que possibilitem uma boa cobertura da parte aérea das plantas com a presença da praga alvo, sem causar escorrimento. Sendo o volume de calda variável de acordo com a espécie de planta.</p><p>-Recomenda-se aplicar nas horas mais frescas do dia, preferencialmente no final da tarde. Evitar aplicação em condição de temperatura acima de 27°C ou na presença de ventos fortes (velocidade acima de 10 Km/hora), bem como com umidade relativa do ar abaixo de 70% ou com alta intensidade de radiação ultravioleta (UV).</p><p>-A escolha dos equipamentos a serem utilizados para aplicação deste produto poderá sofrer alterações a critério do Engenheiro Agrônomo, tomando-se o cuidado de evitar sempre à deriva e perdas do produto por evaporação.</p>','<p>Antes de realizar qualquer mistura de produto na calda de aplicação com o Metarhizonat, é de fundamental importância ver a compatibilidade desses produtos com o fungo Metarhizium anisopliae. Produtos classificado como moderamente tóxicos e tóxicos ao fungo entomopatogênico, prejudicam a performance do microrganismo, matando-o. Em breve, teremos a lista de compatibilidade com a maioria dos defensivos químicos e fertilizantes foliares utilizados na agricultura.<br></p>','<p>- Mantenha o produto em sua embalagem origi­nal, sempre fechada.</p><p>- O local deve ser exclusivo para produtos tóxi­cos, devendo ser isolado de produtos químicos, alimentos, bebidas, rações ou outros materiais.</p><p>- A construção deve ser de alvenaria ou de mate­rial não combustível.</p><p>- O local deve ser ventilado, coberto e ter piso impermeável.</p><p>- Tranque o local, evitando o acesso de pessoas não autorizadas, principalmente crianças.</p><p>- Deve haver sempre embalagens adequadas dis­poníveis para envolver embalagens rompidas ou para o recolhimento de produtos vazados.</p><p>- Em caso de armazéns, deverão ser seguidas as instruções constantes da NBR 9843, da Associa­ção Brasileira de Normas Técnicas - ABNT.</p><p>- Observe as disposições constantes da legisla­ção estadual e municipal.</p><p>- Armazenar sob refrigeração a 27°C, 5°C ou -15°C por até 150 dias.</p>','<p>As embalagens vazias não podem ser transporta­das junto com alimentos, bebidas, medicamentos, rações, animais e pessoas. Devem ser transporta­das em saco plástico transparente (Embalagens Padronizadas – modelo ABNT), devidamente identificado e com lacre, o qual deverá ser adqui­rido nos Canais de Distribuição.<br></p>','<p>O produto apresenta eficiência agronômica comprovada nas culturas da cana-de-açúcar, pastagens, pastagem de capim braquiária e pode ser utilizado em qualquer outra cultura com ocorrência dos alvos biológicos.<br></p>','<p>Nas culturas com a ocorrência do alvo biológico, quando o a população da praga atingir o nível de controle.<br></p>'),(5,'Solunat Japonicum','É o produto à base da bactéria Bradyrhizobium japonicum (SEMIA 5079 e SEMIA 5080), que fixam nitrogênio e promovem o crescimento das plantas de soja.','<p><b><span style=\"font-family: Arial;\">Produto:&nbsp; </span></b><br>Solunat japonicum</p><p><b>Descritivo:&nbsp; </b><br>É o produto à base da bactéria Bradyrhizobium japonicum (SEMIA 5079 e SEMIA 5080), que fixam nitrogênio e promovem o crescimento das plantas de soja.</p><p><b>Categoria:&nbsp; </b><br>Inoculante</p><p><b>Embalagem: </b><br>Caixa com 2 Bags de 5 litros (200 Doses de 50mL).</p><p><b>Concentração: </b><br>7,2 x 109 células viáveis/mL.</p><p><b>Recomendação:&nbsp; </b><br>Indicado para aplicação via semente em soja, sendo recomendado a aplicação de 1 a 3 doses de 50mL para a quantidade de semente a ser utilizada em um hectare.&nbsp;</p><p><b>Composição:&nbsp;</b> <br>7,2 x 109 células viáveis/mL</p><p><b>Cultura Recomendada:</b><br>Soja</p><p><b>Quando utilizar:&nbsp;</b> <br>A utilização do produto deve ser realizada na semeadura.&nbsp;</p><p><b>Como funciona:&nbsp; </b><br>Os rizóbios são bactérias que durante a simbiose localizam-se em estruturas especializadas, os nódulos, que são formados nas raízes de leguminosas. A formação do nódulo inicia após a liberação de flavonóides e isoflavonóides pelas raízes das leguminosas. Esses compostos são reconhecidos pelas bactérias diazotròficas, que aderem e colonizam o pelo radicular. Posteriormente, as bactérias infectam a planta e colonizam as células corticais das raízes. Com a colonização, ocorre a hiperplasia das células, formando assim, a estrutura chamada de nódulo ativo. Após o estabelecimento da simbiose, as bactérias ajudam no processo de assimilação de nitrogênio e conversão da amônia a aminoácidos.&nbsp;&nbsp;</p><p><b>Como aplicar:&nbsp; <br></b>-Antes de realizar a aplicação do inoculante na semente, assegurar que o produto não foi exposto ao Sol e a temperaturas superiores a 30° C;</p><p>- Aplicar em dose adequada conforme a modalidade de uso (em áreas tradicionais de cultivo, no mínimo uma dose/ha de Solunat japonicum via sementes ou 2,5 a 3 doses via sulco de semeadura;</p><p>-Semear imediatamente ou, no máximo, dentro de 24 h após à inoculação;</p><p>- Quando for utilizar produtos em misturas no tratamento de sementes, aplicar o inoculante em segunda operação;</p><p>- Devido à incompatibilidade com químicos no tratamento de sementes, pode-se realizar a inoculação no sulco de semeadura, mas deve-se deixar o tanque para uso exclusivo com o inoculante.</p><p><b>COMPATIBILIDADE:</b></p><p>Antes de realizar qualquer mistura de produto químico na semente ou na calda de aplicação com o Solunat japonicum, é de fundamental importância ver a compatibilidade desses produtos com a bactéria. Em breve, teremos a lista de compatibilidade com a maioria dos defensivos químicos e fertilizantes foliares utilizados na agricultura.</p><p><b>Armazenamento:</b><br>Armazenar o produto em lugar fresco, seco e com temperatura inferior a 28°C</p><p><b>Transporte:</b><br>As embalagens vazias não podem ser transporta­das junto com alimentos, bebidas, medicamentos, rações, animais e pessoas. Devem ser transporta­das em saco plástico transparente (Embalagens Padronizadas – modelo ABNT), devidamente identificado e com lacre, o qual deverá ser adqui­rido nos Canais de Distribuição.</p>','bioforca','solunat-japonicum','solunat.png','2019-12-18 15:54:09','2019-12-18 15:54:09','Inoculante','Caixa com 2 Bags de 5 litros (200 Doses de 50mL)','','Saco aluminizado sanfonado com capacidade de 1 Kg','7,2 x 109 células viáveis/mL','<p>Indicado para aplicação via semente em soja, sendo recomendado a aplicação de 1 a 3 doses de 50mL para a quantidade de semente a ser utilizada em um hectare.&nbsp;<br></p>','7,2 x 109 células viáveis/mL','<p>Os rizóbios são bactérias que durante a simbiose localizam-se em estruturas especializadas, os nódulos, que são formados nas raízes de leguminosas. A formação do nódulo inicia após a liberação de flavonóides e isoflavonóides pelas raízes das leguminosas. Esses compostos são reconhecidos pelas bactérias diazotròficas, que aderem e colonizam o pelo radicular. Posteriormente, as bactérias infectam a planta e colonizam as células corticais das raízes. Com a colonização, ocorre a hiperplasia das células, formando assim, a estrutura chamada de nódulo ativo. Após o estabelecimento da simbiose, as bactérias ajudam no processo de assimilação de nitrogênio e conversão da amônia a aminoácidos.&nbsp; &nbsp;<br></p>','<p>-Antes de realizar a aplicação do inoculante na semente, assegurar que o produto não foi exposto ao Sol e a temperaturas superiores a 30° C;</p><p>- Aplicar em dose adequada conforme a modalidade de uso (em áreas tradicionais de cultivo, no mínimo uma dose/ha de Solunat japonicum via sementes ou 2,5 a 3 doses via sulco de semeadura;</p><p>-Semear imediatamente ou, no máximo, dentro de 24 h após à inoculação;</p><p>- Quando for utilizar produtos em misturas no tratamento de sementes, aplicar o inoculante em segunda operação;</p><p>- Devido à incompatibilidade com químicos no tratamento de sementes, pode-se realizar a inoculação no sulco de semeadura, mas deve-se deixar o tanque para uso exclusivo com o inoculante.</p>','<p>Antes de realizar qualquer mistura de produto químico na semente ou na calda de aplicação com o Solunat japonicum, é de fundamental importância ver a compatibilidade desses produtos com a bactéria. Em breve, teremos a lista de compatibilidade com a maioria dos defensivos químicos e fertilizantes foliares utilizados na agricultura.<br></p>','Armazenar o produto em lugar fresco, seco e com temperatura inferior a 28°C','<p>As embalagens vazias não podem ser transporta­das junto com alimentos, bebidas, medicamentos, rações, animais e pessoas. Devem ser transporta­das em saco plástico transparente (Embalagens Padronizadas – modelo ABNT), devidamente identificado e com lacre, o qual deverá ser adqui­rido nos Canais de Distribuição.<br></p>','Soja','<p>A utilização do produto deve ser realizada na semeadura.<br></p>'),(6,'ADBio Fungi','É adjuvante potencializador da ação do Bovenat e Metarhizonat. Esse produto é recomendado para a pulverização dos fungos entomopatogênicos.','<p><font face=\"Arial\"><span style=\"font-family: Arial;\"><b>Produto:</b>&nbsp;&nbsp;<br></span></font><span style=\"font-family: Arial;\">Adbio Fungi</span></p><p><font face=\"Arial\"><b>Descritivo:&nbsp;&nbsp;</b><br></font><span style=\"font-family: Arial;\">É adjuvante potencializador da ação do Bovenat e Metarhizonat. Esse produto é recomendado para a pulverização dos fungos entomopatogênicos.</span></p><p><font face=\"Arial\"><b>Categoria:&nbsp;&nbsp;</b><br></font><span style=\"font-family: Arial;\">Adjuvante</span></p><p><font face=\"Arial\"><b>Embalagem:&nbsp;</b><br></font><span style=\"font-family: Arial;\">1 e 5 Litros</span></p><p><font face=\"Arial\"><b>Recomendação:&nbsp;&nbsp;</b><br></font><span style=\"font-family: Arial;\">Indicado para pulverização de Bovenat e Metarhizonat.&nbsp;</span><span style=\"font-family: Arial;\">É recomendado 2 mL de ADbio fungi para cada 1 grama da formulação do produto microbiano.&nbsp;</span></p><p><font face=\"Arial\"><b>Cultura Recomendada:</b><br></font><span style=\"font-family: Arial;\">Todas as culturas que for aplicado o Bovenat e Metarhizonat</span></p><p><font face=\"Arial\"><b>Quando utilizar:&nbsp;&nbsp;</b><br></font><span style=\"font-family: Arial;\">Todas as vezes que for pulverizar o Bovenat e Metarhizonat</span></p><p><font face=\"Arial\"><b>Como funciona:&nbsp;&nbsp;</b><br></font><span style=\"font-family: Arial;\">É um adjuvante específico que reduz os efeitos adversos da temperatura e radiação ultravioleta sobre os conídios, além de que, proporciona uma melhor eficiência de controle, pois aumenta o espalhamento e a adesão do fungo no corpo do inseto.</span></p><p><span style=\"font-family: Arial;\"><b>Como aplicar:&nbsp;&nbsp;<br></b></span><span style=\"font-family: Arial;\">- Antes de realizar o preparo da calda de pulverização certificar da limpeza do pulverizador. Caso o pulverizador apresente resíduos de produtos de aplicações anteriores (principalmente fungicida, bactericidas e inseticidas incompatíveis) é de fundamental importância a limpeza do equipamento, pois pode afetar o desempenho do microrganismo;</span></p><p><font face=\"Arial\">-Colocar a formulação do adjuvante potencializador no balde (ADbio fungi);</font></p><p><font face=\"Arial\">-Colocar aos poucos o produto Bovenat ou Metarhizonat no balde e ir misturando;</font></p><p><font face=\"Arial\">-Adicionar água, agitar e em seguida despejar no pulverizador contendo água.</font></p><p><font face=\"Arial\">-Recomenda-se que se inicie a aplicação logo após o preparo da calda de pulverização.</font></p><p><font face=\"Arial\">-Para a aplicação pode-se utilizar pulverizador costal, tratorizado ou aéreo.</font></p><p><font face=\"Arial\">-Efetuar as aplicações de forma que possibilitem uma boa cobertura da parte aérea das plantas com a presença da praga alvo, sem causar escorrimento. Sendo o volume de calda variável de acordo com a espécie de planta.</font></p><p><font face=\"Arial\">-Recomenda-se aplicar nas horas mais frescas do dia, preferencialmente no final da tarde. Evitar aplicação em condição de temperatura acima de 27°C ou na presença de ventos fortes (velocidade acima de 10 Km/hora), bem como com umidade relativa do ar abaixo de 70% ou com alta intensidade de radiação ultravioleta (UV).</font></p><p><font face=\"Arial\">-A escolha dos equipamentos a serem utilizados para aplicação deste produto poderá sofrer alterações a critério do Engenheiro Agrônomo, tomando-se o cuidado de evitar sempre à deriva e perdas do produto por evaporação.</font></p><p><b style=\"font-family: Arial;\">Armazenamento:<br></b><span style=\"font-family: Arial;\">Armazenar o produto em lugar fresco, seco, sem incidência do Sol.</span></p><p><b style=\"font-family: Arial;\">Transporte:<br></b><span style=\"font-family: Arial;\">As embalagens vazias não podem ser transporta­das junto com alimentos, bebidas, medicamentos, rações, animais e pessoas. Devem ser transporta­das em saco plástico transparente (Embalagens Padronizadas – modelo ABNT), devidamente identificado e com lacre, o qual deverá ser adqui­rido nos Canais de Distribuição.</span></p>','bioforca','adbio','adbio.png','2019-12-18 15:54:09','2019-12-18 15:54:09','Adjuvante','1 e 5 Litros','','Saco aluminizado sanfonado com capacidade de 1 Kg','','<p>Indicado para pulverização de Bovenat e Metarhizonat.&nbsp;</p><p>É recomendado 2 mL de ADbio fungi para cada 1 grama da formulação do produto microbiano.&nbsp;</p>',NULL,'<p>É um adjuvante específico que reduz os efeitos adversos da temperatura e radiação ultravioleta sobre os conídios, além de que, proporciona uma melhor eficiência de controle, pois aumenta o espalhamento e a adesão do fungo no corpo do inseto.<br></p>','<p>- Antes de realizar o preparo da calda de pulverização certificar da limpeza do pulverizador. Caso o pulverizador apresente resíduos de produtos de aplicações anteriores (principalmente fungicida, bactericidas e inseticidas incompatíveis) é de fundamental importância a limpeza do equipamento, pois pode afetar o desempenho do microrganismo;</p><p>-Colocar a formulação do adjuvante potencializador no balde (ADbio fungi);</p><p>-Colocar aos poucos o produto Bovenat ou Metarhizonat no balde e ir misturando;</p><p>-Adicionar água, agitar e em seguida despejar no pulverizador contendo água.</p><p>-Recomenda-se que se inicie a aplicação logo após o preparo da calda de pulverização.</p><p>-Para a aplicação pode-se utilizar pulverizador costal, tratorizado ou aéreo.</p><p>-Efetuar as aplicações de forma que possibilitem uma boa cobertura da parte aérea das plantas com a presença da praga alvo, sem causar escorrimento. Sendo o volume de calda variável de acordo com a espécie de planta.</p><p>-Recomenda-se aplicar nas horas mais frescas do dia, preferencialmente no final da tarde. Evitar aplicação em condição de temperatura acima de 27°C ou na presença de ventos fortes (velocidade acima de 10 Km/hora), bem como com umidade relativa do ar abaixo de 70% ou com alta intensidade de radiação ultravioleta (UV).</p><p>-A escolha dos equipamentos a serem utilizados para aplicação deste produto poderá sofrer alterações a critério do Engenheiro Agrônomo, tomando-se o cuidado de evitar sempre à deriva e perdas do produto por evaporação.</p>',NULL,'Armazenar o produto em lugar fresco, seco, sem incidência do Sol.','<p>As embalagens vazias não podem ser transporta­das junto com alimentos, bebidas, medicamentos, rações, animais e pessoas. Devem ser transporta­das em saco plástico transparente (Embalagens Padronizadas – modelo ABNT), devidamente identificado e com lacre, o qual deverá ser adqui­rido nos Canais de Distribuição.<br></p><p>As embalagens vazias não podem ser transporta­das junto com alimentos, bebidas, medicamentos, rações, animais e pessoas. Devem ser transporta­das em saco plástico transparente (Embalagens Padronizadas – modelo ABNT), devidamente identificado e com lacre, o qual deverá ser adqui­rido nos Canais de Distribuição.<br></p><p>As embalagens vazias não podem ser transporta­das junto com alimentos, bebidas, medicamentos, rações, animais e pessoas. Devem ser transporta­das em saco plástico transparente (Embalagens Padronizadas – modelo ABNT), devidamente identificado e com lacre, o qual deverá ser adqui­rido nos Canais de Distribuição.<br></p>','<p>Todas as culturas que for aplicado o Bovenat e Metarhizonat<br></p>','<p>Todas as vezes que for pulverizar o Bovenat e Metarhizonat<br></p>'),(7,'Solunat Brasilense','É o produto à base da bactéria Azospirillum brasilense (Ab-V5 e Ab-V6), promovem crescimento e disponibilização de nitrogênio para as plantas.','<p><b>Produto:&nbsp; </b><br>Solunat brasilense.</p><p><b>Descritivo:&nbsp; </b><br>É o produto à base da bactéria Azospirillum brasilense (Ab-V5 e Ab-V6), promovem crescimento e disponibilização de nitrogênio para as plantas.</p><p><b>Categoria:&nbsp; </b><br>Inoculante.</p><p><b>Embalagem: </b><br>Caixa com 2 Bags de 5 litros (100 Doses de 100mL).</p><p><b>Concentração: </b><br>4 x 108 células viáveis/mL.</p><p><b>Recomendação:&nbsp; </b><br>Indicado para aplicação via semente de milho ou em co-inoculação em soja, sendo recomendado a aplicação de 1 a 3 doses de 100mL para a quantidade de semente a ser utilizada no hectare.&nbsp;</p><p><b>Composição:&nbsp; </b><br>4 x 108 células viáveis/mL.</p><p><b>Cultura Recomendada:</b><br>Milho e Soja.</p><p><b>Quando utilizar:&nbsp; </b><br>A utilização do produto deve ser realizada na semeadura.&nbsp;</p><p><b>Como funciona: </b><br>Vários trabalhos têm conﬁrmado que o Azospirillum produz ﬁtohormônios que estimulam o crescimento das raízes de diversas espécies de plantas. Esses trabalhos comprovaram que o crescimento e desenvolvimento radicular é devido ao Azospirillium brasilense produzirem o ácido indol-acético (AIA), giberilinas e citocininas. Dessa forma, esses hormônios podem ampliar seus efeitos. Assim sendo, quando maior for o sistema radicular, maior é a absorção de água e minerais. Além do mais, a planta tolera os estresses como a seca e salinidade mais facilmente.&nbsp;</p><p><b>Como aplicar:&nbsp;</b>&nbsp;<br></p><p>-Antes de realizar a aplicação do inoculante na semente, assegurar que o produto não foi exposto ao sol e a temperaturas superiores a 30°C;</p><p>- Aplicar em dose adequada conforme a modalidade de uso (em áreas tradicionais de cultivo, no mínimo uma a duas doses/ha de Solunat brasilense via sementes ou 2,5 a 3 doses via sulco de semeadura;</p><p>-Semear imediatamente ou, no máximo, dentro de 24 h após à inoculação;</p><p>- Quando for utilizar produtos em misturas no tratamento de sementes, aplicar o inoculante em segunda operação;</p><p>- Devido à incompatibilidade com químicos no tratamento de sementes, pode-se realizar a inoculação no sulco de semeadura, mas deve-se deixar o tanque para uso exclusivo com o inoculante.</p><p><br></p><p><b>COMPATIBILIDADE:</b><br>Antes de realizar qualquer mistura de produto químico na semente ou na calda de aplicação com o Solunat brasilense, é de fundamental importância ver a compatibilidade desses produtos com a bactéria. Em breve, teremos a lista de compatibilidade com a maioria dos defensivos químicos e fertilizantes foliares utilizados na agricultura.</p><p><b>Armazenamento:</b><br>Armazenar o produto em lugar fresco, seco e com temperatura inferior a 28°C</p><p><b>Transporte:</b><br>As embalagens vazias não podem ser transporta­das junto com alimentos, bebidas, medicamentos, rações, animais e pessoas. Devem ser transporta­das em saco plástico transparente (Embalagens Padronizadas – modelo ABNT), devidamente identificado e com lacre, o qual deverá ser adqui­rido nos Canais de Distribuição.</p>','bioforca','solunat-brasilense','solunat.png','2019-12-18 15:54:09','2019-12-18 15:54:09','Inoculante','Caixa com 2 Bags de 5 litros (100 Doses de 100mL)','','Saco aluminizado sanfonado com capacidade de 1 Kg','4 x 108 células viáveis/mL','<p>Indicado para aplicação via semente de milho ou em co-inoculação em soja, sendo recomendado a aplicação de 1 a 3 doses de 100mL para a quantidade de semente a ser utilizada no hectare.&nbsp;<br></p>','4 x 108 células viáveis/mL.','<p>Vários trabalhos têm conﬁrmado que o Azospirillum produz ﬁtohormônios que estimulam o crescimento das raízes de diversas espécies de plantas. Esses trabalhos comprovaram que o crescimento e desenvolvimento radicular é devido ao Azospirillium brasilense produzirem o ácido indol-acético (AIA), giberilinas e citocininas. Dessa forma, esses hormônios podem ampliar seus efeitos. Assim sendo, quando maior for o sistema radicular, maior é a absorção de água e minerais. Além do mais, a planta tolera os estresses como a seca e salinidade mais facilmente.&nbsp;<br></p>','<p>-Antes de realizar a aplicação do inoculante na semente, assegurar que o produto não foi exposto ao sol e a temperaturas superiores a 30°C;</p><p>- Aplicar em dose adequada conforme a modalidade de uso (em áreas tradicionais de cultivo, no mínimo uma a duas doses/ha de Solunat brasilense via sementes ou 2,5 a 3 doses via sulco de semeadura;</p><p>-Semear imediatamente ou, no máximo, dentro de 24 h após à inoculação;</p><p>- Quando for utilizar produtos em misturas no tratamento de sementes, aplicar o inoculante em segunda operação;</p><p>- Devido à incompatibilidade com químicos no tratamento de sementes, pode-se realizar a inoculação no sulco de semeadura, mas deve-se deixar o tanque para uso exclusivo com o inoculante.</p>','<p>Antes de realizar qualquer mistura de produto químico na semente ou na calda de aplicação com o Solunat brasilense, é de fundamental importância ver a compatibilidade desses produtos com a bactéria. Em breve, teremos a lista de compatibilidade com a maioria dos defensivos químicos e fertilizantes foliares utilizados na agricultura.<br></p>','Armazenar o produto em lugar fresco, seco e com temperatura inferior a 28°C','<p>As embalagens vazias não podem ser transporta­das junto com alimentos, bebidas, medicamentos, rações, animais e pessoas. Devem ser transporta­das em saco plástico transparente (Embalagens Padronizadas – modelo ABNT), devidamente identificado e com lacre, o qual deverá ser adqui­rido nos Canais de Distribuição.<br></p>','Milho e Soja.','A utilização do produto deve ser realizada na semeadura. ');
/*!40000 ALTER TABLE `contents` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `contents_has_contents`
--

DROP TABLE IF EXISTS `contents_has_contents`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `contents_has_contents` (
  `contents_id` int(11) NOT NULL,
  `contents_child_id` int(11) NOT NULL,
  PRIMARY KEY (`contents_id`,`contents_child_id`),
  KEY `fk_contents_has_contents_contents2_idx` (`contents_child_id`),
  KEY `fk_contents_has_contents_contents1_idx` (`contents_id`),
  CONSTRAINT `fk_contents_has_contents_contents1` FOREIGN KEY (`contents_id`) REFERENCES `contents` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_contents_has_contents_contents2` FOREIGN KEY (`contents_child_id`) REFERENCES `contents` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `contents_has_contents`
--

LOCK TABLES `contents_has_contents` WRITE;
/*!40000 ALTER TABLE `contents_has_contents` DISABLE KEYS */;
/*!40000 ALTER TABLE `contents_has_contents` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `contents_images`
--

DROP TABLE IF EXISTS `contents_images`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `contents_images` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `description` text,
  `type` varchar(45) DEFAULT NULL,
  `order` varchar(45) DEFAULT NULL,
  `image` text,
  `contents_id` int(11) NOT NULL,
  `path` text,
  PRIMARY KEY (`id`,`contents_id`),
  KEY `fk_contents_images_contents_idx` (`contents_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `contents_images`
--

LOCK TABLES `contents_images` WRITE;
/*!40000 ALTER TABLE `contents_images` DISABLE KEYS */;
/*!40000 ALTER TABLE `contents_images` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `informations`
--

DROP TABLE IF EXISTS `informations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `informations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `address` text,
  `number` varchar(45) DEFAULT NULL,
  `complement` varchar(255) DEFAULT NULL,
  `district` varchar(255) DEFAULT NULL,
  `zipcode` varchar(45) DEFAULT NULL,
  `city` varchar(255) DEFAULT NULL,
  `state` varchar(255) DEFAULT NULL,
  `phone1` varchar(45) DEFAULT NULL,
  `email` text,
  `about_us` text,
  `video` varchar(255) DEFAULT NULL,
  `opening_hours` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `informations`
--

LOCK TABLES `informations` WRITE;
/*!40000 ALTER TABLE `informations` DISABLE KEYS */;
INSERT INTO `informations` VALUES (1,'Rod. Assis Chateaubriand','s/n','Km 144 + 500m Conj. 02','Zona Rural','15400 000','Olímpia','SP','17 3279-4950','contato@bionatagro.com','A Bionat é uma fábrica de soluções biológicas com parceiras com o Instituto Biológico de Campinas, Embrapa Recursos Genéticos e Biotecnologia (Cenargem) e a Empresa Brasileira de Pesquisa e Inovação Industrial (Embrapa).','https://www.youtube.com/watch?v=X6Xbpwpfn9w','Segunda a sexta das 8:00 às 18:00');
/*!40000 ALTER TABLE `informations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `migrations`
--

LOCK TABLES `migrations` WRITE;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` VALUES (1,'2014_10_12_000000_create_users_table',1),(2,'2014_10_12_100000_create_password_resets_table',1);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `newsletters`
--

DROP TABLE IF EXISTS `newsletters`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `newsletters` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(45) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `newsletters`
--

LOCK TABLES `newsletters` WRITE;
/*!40000 ALTER TABLE `newsletters` DISABLE KEYS */;
INSERT INTO `newsletters` VALUES (1,'sdsdsd@teste.com.br','2019-12-19 20:06:38','2019-12-19 20:06:38'),(2,'lemajstor@gmail.com','2019-12-19 20:07:18','2019-12-19 20:07:18'),(3,'edinaldo@agencialed.com.br','2019-12-19 20:15:56','2019-12-19 20:15:56'),(4,'atendimento@geo18k.com.br','2019-12-19 20:19:59','2019-12-19 20:19:59'),(5,'maniusa17@hotmail.com','2019-12-19 20:20:35','2019-12-19 20:20:35');
/*!40000 ALTER TABLE `newsletters` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `password_resets`
--

DROP TABLE IF EXISTS `password_resets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `password_resets` (
  `email` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`(191))
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `password_resets`
--

LOCK TABLES `password_resets` WRITE;
/*!40000 ALTER TABLE `password_resets` DISABLE KEYS */;
INSERT INTO `password_resets` VALUES ('enzo.nagata@gmail.com','$2y$10$F87xZsXNmV3On.AEylyz3esP.VH3c4P7mZUpv0atsKI7D9KyLMvXS','2019-09-13 21:50:52');
/*!40000 ALTER TABLE `password_resets` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `posts`
--

DROP TABLE IF EXISTS `posts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `posts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` text NOT NULL,
  `short_description` text NOT NULL,
  `content` text NOT NULL,
  `url` varchar(255) DEFAULT NULL,
  `image` varchar(45) NOT NULL DEFAULT 'null',
  `feat_image` varchar(45) NOT NULL DEFAULT 'null',
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `posts`
--

LOCK TABLES `posts` WRITE;
/*!40000 ALTER TABLE `posts` DISABLE KEYS */;
INSERT INTO `posts` VALUES (23,'KIMBERLIT AGROCIÊNCIAS INAUGURA FÁBRICA NO INTERIOR DE SÃO PAULO','A Kimberlit Agrociências ganha nova fábrica de defensivos biológicos Bionat. A inauguração ocorreu nesta terça-feira, no interior de São Paulo, na cidade de Olímpia.','<p>A Kimberlit Agrociências ganha nova fábrica de defensivos biológicos Bionat. A inauguração ocorreu nesta terça-feira, no interior de São Paulo, na cidade de Olímpia.</p><p>O mercado de fertilizantes deu um grande salto no ano de 2018, com faturamento de R$ 464,5 milhões e perspectiva de avanço de 20% no ano de 2019. Esse cenário influenciou na ampliação do parque fabril da Kimberlit.<br></p><p>A primeira fase de aportes será destinada ao desenvolvimento e a montagem da fábrica, serão gastos em torno de R$ 25 milhões. Para sua expansão R$ 20 milhões serão investidos e, por último, R$ 2,3 milhões virão da Financiadora de Estudos e Projetos (Finep).<br></p><p>Foi calculado pela empresa um aumento de 25% no faturamento para o ano de 2019, além da chegada de 32 novos produtos para o segmento. Com o mercado cada vez mais promissor, o número de produtos biológicos no Brasil disparou. Em 2010 haviam apenas 19 produtos biológicos no país e no início desse ano 200 produtos já estão disponíveis.&nbsp;</p>','kimberlit-agrociencias-inaugura-fabrica-no-interior-de-sao-paulo','post_5e00cfd486c88.jpeg','post_min_5e00cfd4869f8.jpeg','2019-12-23 14:31:48','2019-12-23 14:31:48'),(24,'ENTREVISTA NO PROGRAMA DIA DIA RURAL, 19.07, CANAL TERRA VIVA','Entrevista no programa dia dia Rural, 19.07, canal terra viva','<p>Entrevista no programa dia dia Rural, 19.07, canal terra viva</p><p>Luciano de Gissi, diretor da Bionat</p><p><br></p><p><iframe frameborder=\"0\" src=\"//www.youtube.com/embed/nVIC_wKcSCo\" width=\"640\" height=\"360\" class=\"note-video-clip\"></iframe><br></p><p><br></p><p><br></p>','entrevista-no-programa-dia-dia-rural-19-07-canal-terra-viva','post_5e00d1ee414a5.jpeg','post_min_5e00d1ee41275.jpeg','2019-12-23 14:40:46','2019-12-23 14:40:46'),(25,'BIONAT É A NOVA OPÇÃO EM SOLUÇÕES BIOLÓGICAS','A Bionat saiu em um artigo na revista Campos e Negócios - Edição 201 / Dezembro 2019. Confira o artigo na íntegra.','<p>Kimberlit, empresa líder no segmento de fertilizantes especiais, especializada em nutrição e fisiologia de plantas, de olho em um mercado crescente, investe na produção de defensivos biológicos e inaugura a Bionat, fábrica que desenvolverá, produzirá e comercializará soluções biológicas inteligentes, sustentáveis e eficientes. Segundo dados da Associação Brasileira de Controle Biológico (ABC-Bio), o segmento faturou R$ 464,5 milhões em 2018 e a perspectiva é de crescer 20% em 2019.</p><p>“A Bionat surge a partir da visão de futuro do agronegócio mundial e das necessidades manifestadas pelos agropecuaristas de todas as regiões por produtos que potencializem os resultados no campo, e tenham um caráter sustentável.</p><p>Os números mostram que esse é um mercado de grande potencial; 57% dos produtores brasileiros dizem desconhecer os biodefensivos, de acordo com levantamento realizado pela ABC-Bio; temos um mercado extenso para desbravar”, afirma Luciano de Gissi, diretor na Bionat.</p><p>Foram cinco anos até que a nova empresa fosse concluída. Em 2016 o projeto foi minuciosamente estudado, pesquisado, trabalhado e detalhado com o auxílio de consultoria especializada e de pesquisadores do Instituto Biológico, da UFSCAR (Universidade Federal de São Carlos) e da ESALQ (Escola Superior de Agricultura da Universidade de São Paulo).<br></p><p>Com projeto arquitetônico e industrial aprovado em 2017, a primeira fase a unidade fabril da Bionat - localizada em Olímpia (SP) - está distribuída em 400 m2 de edificações com capacidade de produção de bioprodutos para tratamento de até 350 mil hectares por ano e contará com 8 funcionários.</p><p>A fábrica está aparelhada com equipamentos de tecnologia de ponta para a produção de fungos e bactérias, como um biorreator de última geração que pode ser controlado pelo celular, além de possuir processo de incubação e extração com alto grau de automação e oferecer um ambiente totalmente asséptico para reduzir a possibilidade de<br></p><p>contaminação – como parede revestida com tinta hospitalar antibacteriana, piso emborrachado e lavável, mobiliário produção em inox 316, com o objetivo promover um espaço saudável para a produção.</p><p>A Bionat está funcionando em fase de testes e tem previsão de início de produção em escala a partir agosto, com a liberação de registro feita pelo Mapa(- Ministério da Agricultura, Pecuária e Abastecimento), oferecendo ao mercado dois defensivos que têm como foco o combate às pragas cigarrinha e mosca-branca: o Metarhizium anisopliae,<br></p><p>fungo que é comprovadamente eficaz controlador biológico de várias espécies e cigarrinhas que ocorrem na agricultura, produzido em arroz, sob condição</p><p>de excelente de assepsia, rigoroso controle de qualidade a fim de garantir a ausência de contaminantes, eficiência na germinação e virulência dos conídios; e Beauveria bassiana, inseticida microbiológico indicado para o controle de insetos e ácaros pragas nas mais diversas culturas, tais como a mosca-branca, ácaro rajado, etc.</p><p><a href=\"http://local.bionat.com.br/bionat_artigo.pdf\" target=\"_blank\">Clique aqui para baixar o artigo completo</a><br></p>','bionat-e-a-nova-opcao-em-solucoes-biologicas','post_5e00d305133e6.jpeg','post_min_5e00d30512f4f.jpeg','2019-12-23 14:45:25','2019-12-23 14:45:25');
/*!40000 ALTER TABLE `posts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'Enzo Nagata','enzo.nagata@gmail.com',NULL,'$2y$10$HUuXgtcUoSVCKRnGZ7g4WOIogDOepbY4ZRb5M2ZMyJHWSvK9fMD0W','t95qJAaDIFb8MAzlxNaRAyeJqAZcJB43SsC5Bz7c3woDaNyvLsVg6v0rOqfM','2019-08-23 14:40:17','2019-08-23 14:40:17'),(2,'Edinaldo','edinaldo@agencialed.com.br',NULL,'$2y$10$HYROoUsNkqgAz65RW0Ct4uEX9vDIMH8KlQlmrKHigyRNfYUwzOUvG',NULL,'2019-09-13 22:19:59','2019-09-13 22:19:59'),(3,'admin','digital@agencialed.com.br',NULL,'$2y$10$qKyewisFIEyEgJSF5bTCFOtsKOyUFghosvMP1AkJBB4v6lPUchoa6',NULL,'2019-12-12 21:57:15','2019-12-12 21:57:15');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping events for database 'bionat'
--

--
-- Dumping routines for database 'bionat'
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-01-10 11:51:15
